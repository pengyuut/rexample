#include <Rcpp.h>

using namespace Rcpp;

RcppExport SEXP test(SEXP xs) {
  double x = as<double>(xs);
  return wrap(x); 
}
