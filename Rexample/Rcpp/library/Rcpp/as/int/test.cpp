#include <Rcpp.h>

using namespace Rcpp;

RcppExport SEXP test(SEXP ns) {
	int n = as<int>(ns);
	return wrap(n); 
}
