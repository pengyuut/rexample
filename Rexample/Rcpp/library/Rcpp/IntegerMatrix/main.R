suppressPackageStartupMessages(library(inline))
src='
Rcpp::IntegerVector mat =
Rcpp::clone<Rcpp::IntegerMatrix>(mx);
std::transform(mat.begin(), mat.end(),
  mat.begin(), ::sqrt);
return mat;
'
fun=cxxfunction(signature(mx="numeric"), src,
  plugin="Rcpp")
orig=matrix((1:9)*.5, 3, 3)
fun(orig)
