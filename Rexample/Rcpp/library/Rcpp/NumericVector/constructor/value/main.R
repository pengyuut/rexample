library(inline)
src='
Rcpp::NumericVector vec(10, 2.);
return Rcpp::wrap(vec);
'
fun=cxxfunction(
  signature()
  , src
  , plugin='Rcpp'
  )
fun()

