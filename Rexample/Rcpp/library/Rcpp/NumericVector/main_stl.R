library(inline)
src='
Rcpp::IntegerVector vec(vx);
int sum = std::accumulate(vec.begin(), vec.end(), 1, std::plus<int>());
return Rcpp::wrap(sum);
'
fun=cxxfunction(
  signature(vx='integer')
  , src
  , plugin='Rcpp'
  )
fun(1L:10L)
