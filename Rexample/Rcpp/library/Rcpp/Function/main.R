library(inline)
fun=cxxfunction(
	sig=signature(
		x='numeric'
		, f='function'
		)
	, body='
	NumericVector xx = Rcpp::as<NumericVector>(x);
	Function ff = Rcpp::as<Function>(f);
	NumericVector res = ff(xx);
	return wrap(res);
	'
	, plugin='Rcpp'
	)
x=rnorm(1e5)
fivenum(x)
fun(x, fivenum)
