#include <Rcpp.h>

using namespace Rcpp;

RcppExport SEXP func(SEXP vx) {
  IntegerVector vec(vx);
  int sum = 0;
  for (int i=0; i<vec.size(); i++) {
    sum += vec[i];
  }
  return wrap(sum);
}
