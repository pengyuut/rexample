From `?Startup`

~~~
Then, unless ‘--no-init-file’ was given, R searches for a user
profile, a file of R code.  The path of this file can be specified
by the ‘R_PROFILE_USER’ environment variable (and tilde expansion
will be performed).  If this is unset, a file called ‘.Rprofile’
is searched for in the current directory or in the user's home
directory (in that order).  The user profile file is sourced into
the workspace.
~~~
