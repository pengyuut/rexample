#!/usr/bin/env bash

set -v
R -q --no-save --no-restore --no-init-file <<EOF
print(options('echo'))
EOF

R -q --no-save --no-restore --no-init-file --slave <<EOF
print(options('echo'))
EOF
