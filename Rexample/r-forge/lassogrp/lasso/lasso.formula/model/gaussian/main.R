library(lassogrp)
data(quine, package='MASS')
rr <- lasso(
  Days~Eth+Sex+Age+Lrn
  , data = quine
  )
rr
names(rr)

