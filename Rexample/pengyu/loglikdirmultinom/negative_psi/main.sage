#!/usr/bin/env sage

def loglikdirmultinom(x, p, psi):
  alpha=[i/psi for i in p]
  N=sum(x)
  A=sum(alpha)
  #print 'A', A
  #print 'N', N
  tmp2=gamma(A)/gamma(A+N)
  #print 'tmp2', tmp2
  tmp3=prod([gamma(alpha_i + x_i)/gamma(alpha_i) for alpha_i, x_i in zip(alpha, x)])
  #print 'tmp3', tmp3
  #return log(abs(tmp2*tmp3))
  return log(abs(tmp2*tmp3))

#p=[1/2, 1/2]
#psi=1/200
#x=[5,5]
#print n(loglikdirmultinom(x, p, psi), digits=22)

#psi=1./200.5
x=[5, 5]
#psi=-1./200.5
psi=-1./100200.5
print n(loglikdirmultinom(x, p, psi), digits=22)
#loglikdirmultinom(x, p, psi)
#
for i in range(-100, 100):
  psi = i * .00378 + .002288
  print psi, n(loglikdirmultinom(x, p, psi), digits=22)

xmin=-0.114892000000000
xmax=-0.107332000000000
for i in range(-100, 100):
  psi = i * (xmax-xmin)/200 + xmin
  print psi, n(loglikdirmultinom(x, p, psi), digits=22)

xmin=-0.9114892000000000
xmax=0.0107332000000000
for i in range(0, 200):
  psi = i * (xmax-xmin)/200 + xmin
  print psi, n(loglikdirmultinom(x, p, psi), digits=22)

