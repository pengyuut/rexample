suppressPackageStartupMessages(library(loglikdirmultinom))

p=c(.2, .3, .5)
n=c(3, 5, 7)
psi=.01
loglikdirmultinom_mesh(n, p, psi)
