suppressPackageStartupMessages(library(loglikdirmultinom))

p1=c(.2, .3, .5)
p2=c(.3, .3, .4)
psi1=.01
psi2=.1
n1=c(3, 5, 7)
n2=c(8, 3, 5)
loglikdirmultinom_mesh(n1, p1, psi1) + loglikdirmultinom_mesh(n2, p2, psi2)
p=rbind(p1, p2)
psi=c(psi1, psi2)
n=rbind(n1, n2)
loglikdirmultinom_mesh_multiple(n, p, psi)

