\name{rcpp_hello_world_R}
\alias{rcpp_hello_world_R}
\title{Rcpp Hello World Example}
\usage{
  rcpp_hello_world_R()
}
\arguments{
  \item{df}{data frame to reorder}

  \item{...}{expressions evaluated in the context of
  \code{df} and then fed to \code{\link{order}}}
}
\description{
  This function completes the subsetting, transforming and
  ordering triad with a function that works in a similar
  way to \code{\link{subset}} and \code{\link{transform}}
  but for reordering a data frame by its columns. This
  saves a lot of typing!
}
\examples{
rcpp_hello_world()
}
\keyword{manip}

