#include <R.h>

static char *sfunction;
void dosimp(char** funclist,double *start, double *stop,long *n,double *answer) {
  double sfunc(double);
  double simp(double(*)(),double,double,long);
  sfunction = funclist[0];
  *answer = simp((double(*)())sfunc,*start,*stop,*n);
}

double sfunc(double x) {
  char *modes[1];
  char *arguments[1];
  double *result;
  long lengths[2];
  lengths[0] = (long)1;
  arguments[0] = (char*)&x;
  modes[0] = "double";
  call_R(sfunction,(long)1,arguments,modes,lengths,
      (char*)0,(long)1,arguments);
  result = (double*)arguments[0];
  return(*result);
}

double simp(double(*func)(),double start,double stop,long n) {
  double mult,x,t,t1,inc;
  long i;
  inc = (stop - start) / (double)n;
  t = func(x = start);
  mult = 4.;
  for(i=1;i<n;i++)
  {x += inc;
    t += mult * func(x);
    mult = mult == 4. ? 2. : 4.; }
  t += func(stop);
  return(t * inc / 3.);
}
