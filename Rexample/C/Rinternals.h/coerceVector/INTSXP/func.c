#include <R.h>
#include <Rinternals.h>
#include <Rmath.h>

SEXP func(SEXP x) {
  return coerceVector(x, INTSXP);
}
