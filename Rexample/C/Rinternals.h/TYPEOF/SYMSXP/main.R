dyn.load('func.so')
f=function(x) {
  invisible(.Call('func', x))
}
f(NULL)
f(quote(x))
f(new.env())
f(T)
f(1L)
f(1)
f(1+1i)
f('a')
f(list(1))
