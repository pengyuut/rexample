#include <R.h>
#include <Rinternals.h>
//#include <Rmath.h>

SEXP func() {
  SEXP x;
  PROTECT(x = allocVector(INTSXP, 2));
  INTEGER(x)[0] = 0;
  INTEGER(x)[1] = 1;
  UNPROTECT(1);
  return x;
}
