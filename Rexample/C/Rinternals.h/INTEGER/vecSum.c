#include <Rinternals.h>

SEXP vecSum(SEXP Rvec) {
  int i, n;
  int *vec, value = 0;
  vec = INTEGER(Rvec);
  n = length(Rvec);
  for (i = 0; i < n; i++)
    value += vec[i];
  printf("The value is: %d \n", value);
  return R_NilValue;
}

