~~~
src/include/Rinternals.h
571:LibExtern SEXP  R_NilValue;     /* The nil object */

src/main/memory.c

1912 /* InitMemory : Initialise the memory to be used in R. */
1913 /* This includes: stack space, node space and vector space */
1914 
1915 #define PP_REDZONE_SIZE 1000L
1916 static int R_StandardPPStackSize, R_RealPPStackSize;
1917 
1918 void attribute_hidden InitMemory()
1919 {
1920     int i;
1921     int gen;
1922 
1923     init_gctorture();
1924     init_gc_grow_settings();
1925 
1926     gc_reporting = R_Verbose;
1927     R_StandardPPStackSize = R_PPStackSize;
1928     R_RealPPStackSize = R_PPStackSize + PP_REDZONE_SIZE;
1929     if (!(R_PPStack = (SEXP *) malloc(R_RealPPStackSize * sizeof(SEXP))))
1930         R_Suicide("couldn't allocate memory for pointer stack");
1931     R_PPStackTop = 0;
1932 #if VALGRIND_LEVEL > 1
1933     VALGRIND_MAKE_NOACCESS(R_PPStack+R_PPStackSize, PP_REDZONE_SIZE);
1934 #endif
1935     vsfac = sizeof(VECREC);
1936     R_VSize = (R_VSize + 1)/vsfac;
1937     if (R_MaxVSize < R_SIZE_T_MAX) R_MaxVSize = (R_MaxVSize + 1)/vsfac;
1938 
1939     UNMARK_NODE(&UnmarkedNodeTemplate);
1940 
1941     for (i = 0; i < NUM_NODE_CLASSES; i++) {
1942       for (gen = 0; gen < NUM_OLD_GENERATIONS; gen++) {
1943         R_GenHeap[i].Old[gen] = &R_GenHeap[i].OldPeg[gen];
1944         SET_PREV_NODE(R_GenHeap[i].Old[gen], R_GenHeap[i].Old[gen]);
1945         SET_NEXT_NODE(R_GenHeap[i].Old[gen], R_GenHeap[i].Old[gen]);
1946 
1947 #ifndef EXPEL_OLD_TO_NEW
1948         R_GenHeap[i].OldToNew[gen] = &R_GenHeap[i].OldToNewPeg[gen];
1949         SET_PREV_NODE(R_GenHeap[i].OldToNew[gen], R_GenHeap[i].OldToNew[gen]);
1950         SET_NEXT_NODE(R_GenHeap[i].OldToNew[gen], R_GenHeap[i].OldToNew[gen]);
1951 #endif
1952 
1953         R_GenHeap[i].OldCount[gen] = 0;
1954       }
1955       R_GenHeap[i].New = &R_GenHeap[i].NewPeg;
1956       SET_PREV_NODE(R_GenHeap[i].New, R_GenHeap[i].New);
1957       SET_NEXT_NODE(R_GenHeap[i].New, R_GenHeap[i].New);
1958     }
1959 
1960     for (i = 0; i < NUM_NODE_CLASSES; i++)
1961         R_GenHeap[i].Free = NEXT_NODE(R_GenHeap[i].New);
1962 
1963     SET_NODE_CLASS(&UnmarkedNodeTemplate, 0);
1964     orig_R_NSize = R_NSize;
1965     orig_R_VSize = R_VSize;
1966 
1967     /* R_NilValue */
1968     /* THIS MUST BE THE FIRST CONS CELL ALLOCATED */
1969     /* OR ARMAGEDDON HAPPENS. */
1970     /* Field assignments for R_NilValue must not go through write barrier
1971        since the write barrier prevents assignments to R_NilValue's fields.
1972        because of checks for nil */
1973     GET_FREE_NODE(R_NilValue);
1974     R_NilValue->sxpinfo = UnmarkedNodeTemplate.sxpinfo;
1975     TYPEOF(R_NilValue) = NILSXP;
1976     CAR(R_NilValue) = R_NilValue;
1977     CDR(R_NilValue) = R_NilValue;
1978     TAG(R_NilValue) = R_NilValue;
1979     ATTRIB(R_NilValue) = R_NilValue;
1980 
1981     R_BCNodeStackBase = (SEXP *) malloc(R_BCNODESTACKSIZE * sizeof(SEXP));
1982     if (R_BCNodeStackBase == NULL)
1983         R_Suicide("couldn't allocate node stack");
1984 #ifdef BC_INT_STACK
1985     R_BCIntStackBase =
1986       (IStackval *) malloc(R_BCINTSTACKSIZE * sizeof(IStackval));
1987     if (R_BCIntStackBase == NULL)
1988         R_Suicide("couldn't allocate integer stack");
1989 #endif
1990     R_BCNodeStackTop = R_BCNodeStackBase;
1991     R_BCNodeStackEnd = R_BCNodeStackBase + R_BCNODESTACKSIZE;
1992 #ifdef BC_INT_STACK
1993     R_BCIntStackTop = R_BCIntStackBase;
1994     R_BCIntStackEnd = R_BCIntStackBase + R_BCINTSTACKSIZE;
1995 #endif
1996 
1997     R_weak_refs = R_NilValue;
1998 
1999     R_HandlerStack = R_RestartStack = R_NilValue;
2000 
2001     /*  Unbound values which are to be preserved through GCs */
2002     R_PreciousList = R_NilValue;
2003     
2004     /*  The current source line */
2005     R_Srcref = R_NilValue;
2006 }
~~~

