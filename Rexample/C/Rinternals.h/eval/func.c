#include <Rinternals.h>

SEXP func(SEXP f, SEXP x, SEXP e) {
  SEXP R_fcall = lang2(f, x);
  return eval(R_fcall, e);
}

