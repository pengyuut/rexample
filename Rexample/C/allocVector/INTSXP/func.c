#include <R.h>
#include <Rinternals.h>
#include <Rmath.h>

SEXP func() {
  SEXP Rval;
  PROTECT(Rval = allocVector(INTSXP, 10));
  for (int i = 0; i < 10; i++)
    INTEGER(Rval)[i] = i;
  UNPROTECT(1);
  return Rval;
}
