#!/usr/bin/env bash

g++ -I/Library/Frameworks/R.framework/Versions/2.15/Resources/include/ -c main.cpp
#g++ -o main.exe main.o -L/Library/Frameworks/R.framework/Versions/2.15/Resources/lib -lRmath
g++ -o main.exe main.o -L/Library/Frameworks/R.framework/Versions/2.15/Resources/lib -lR
./main.exe
