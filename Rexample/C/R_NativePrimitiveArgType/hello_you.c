#include <stdio.h>
#include "R.h"
#include "Rdefines.h"
#include "R_ext/Rdynload.h"

void hello_you(char **name) {
    printf("Hello, %s!\n", name[0]);
}

R_NativePrimitiveArgType hello_youArgs[1] = {STRSXP};
R_CMethodDef cMethods[] =
{
    {"hello_you", (DL_FUNC)&hello_you, 1, hello_youArgs},
    {NULL,NULL, 0}
};

void R_init_hello_you(DllInfo *dll)
{
    R_registerRoutines(dll,cMethods,NULL,NULL,NULL);
}
