#include <R.h>
#include <Rdefines.h>
extern "C" SEXP listex(){
  SEXP res = PROTECT( allocVector( VECSXP, 2 ) ) ;
  SEXP x1 = PROTECT( allocVector( REALSXP, 2 ) ) ;
  SEXP x2 = PROTECT( allocVector( INTSXP, 2 ) ) ;
  SEXP names = PROTECT( mkString( "foobar" ) ) ;
  double* px1 = REAL(x1);
  px1[0] = 0.5;
  px1[1] = 1.5;
  int* px2 = INTEGER(x2);
  px2[0] = 2;
  px2[1] = 3;
  SET_VECTOR_ELT( res, 0, x1 ) ;
  SET_VECTOR_ELT( res, 1, x2 ) ;
  setAttrib(res, install("class"), names) ;
  UNPROTECT(4) ;
  return res ;
}
