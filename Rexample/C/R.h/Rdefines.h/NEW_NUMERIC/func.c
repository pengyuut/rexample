#include <R.h>
#include <Rdefines.h>

SEXP func() {
  SEXP x;
  PROTECT(x = NEW_NUMERIC(2));
  NUMERIC_POINTER(x)[0] = 0;
  NUMERIC_POINTER(x)[1] = 1;
  UNPROTECT(1);
  return x;
}
