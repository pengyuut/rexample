load(infile)
library(DESeq)
assign(
    outvar_name
    , sizeFactors(get(invar_name))
    )

save(list=outvar_name, file=outfile)

