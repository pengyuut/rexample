parametricDispersionFit=function(means, disps) 
{
  coefs=c(.1, 1)
  iter=0   
  while(T) {
    residuals=disps / (coefs[1] + coefs[2] / means)
    good=which((residuals > 1e-4) & (residuals < 15))
    fit=glm(disps[good] ~ I(1/means[good]), family=Gamma(link="identity"), start=coefs)
    oldcoefs=coefs   
    coefs=coefficients(fit)
    if(!all(coefs > 0))
      stop("Parametric dispersion fit failed. Try a local fit and/or a pooled estimation. (See '?estimateDispersions')")
    if(sum(log(coefs / oldcoefs)^2) < 1e-6)
      break
    iter=iter + 1
    if(iter > 10) {
      warning("Dispersion fit did not converge.")
      break }
  }

  names(coefs)=c("asymptDisp", "extraPois")
  ans=function(q)
    coefs[1] + coefs[2] / q
  attr(ans, "coefficients")=coefs
  ans
}   


