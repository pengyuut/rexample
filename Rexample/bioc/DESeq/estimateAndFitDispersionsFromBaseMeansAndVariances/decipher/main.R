estimateAndFitDispersionsFromBaseMeansAndVariances <- function( means, 
   variances, sizeFactors, fitType = c( "parametric", "local" ),
   locfit_extra_args=list(), lp_extra_args=list(), adjustForBias=TRUE ) {
   
   fitType <- match.arg( fitType )

   xim <- mean( 1/sizeFactors )
   dispsAll <- ( variances - xim * means ) / means^2
   
   variances <- variances[ means > 0 ]
   disps <- dispsAll[ means > 0 ]
   means <- means[ means > 0 ]

   if( adjustForBias )
      disps <- adjustScvForBias( disps, length( sizeFactors ) )
   
   if( fitType == "local" ) {
   
      fit <- do.call( "locfit", c( 
         list( 
            variances ~ do.call( "lp", c( list( log(means) ), lp_extra_args ) ),
            family = "gamma" ), 
         locfit_extra_args ) )
      
      rm( means )
      rm( variances )
      
      if( adjustForBias )
         ans <- function( q )
            adjustScvForBias( 
               pmax( ( safepredict( fit, log(q) ) - xim * q ) / q^2, 1e-8 ),
               length(sizeFactors) )
      else
         ans <- function( q )
            pmax( ( safepredict( fit, log(q) ) - xim * q ) / q^2, 1e-8 )
            
      # Note: The 'pmax' construct above serves to limit the overdispersion to a minimum
      # of 10^-8, which should be indistinguishable from 0 but ensures numerical stability.

   } else if( fitType == "parametric" ) {

      ans <- parametricDispersionFit( means, disps )
   
   } else
      stop( "Unknown fitType." )
   
   attr( ans, "fitType" ) <- fitType
   list( disps=dispsAll, dispFunc=ans )
} 
