load(infile)
library(DESeq)
assign(
    outvar_name
    , estimateSizeFactorsForMatrix(get(invar_name))
    )

save(list=outvar_name, file=outfile)

#geomeans=exp(rowMeans(log(cnts)))
#result1=apply(cnts, 2, function(x) median((x/geomeans)[geomeans > 0]))
#
#all.equal(result, result1)

