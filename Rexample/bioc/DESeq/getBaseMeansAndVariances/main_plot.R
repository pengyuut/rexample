suppressPackageStartupMessages(library(DESeq))
cds=makeExampleCountDataSet()
cds=estimateSizeFactors(cds)
bmv=getBaseMeansAndVariances(counts(cds), sizeFactors(cds))

library(ggplot2)
p=qplot(baseMean, baseVar, data=bmv)
ggsave(p, file='main_plot.png')


