suppressPackageStartupMessages(library(mouse4302.db))
x=mouse4302ENSEMBL2PROBE
show(x)
summary(x)
direction(x)

length(x)
Llength(x)
Rlength(x)

head(keys(x))
head(Lkeys(x))
head(Rkeys(x))

count.mappedkeys(x)
count.mappedLkeys(x)
count.mappedRkeys(x)

head(mappedkeys(x))
head(mappedLkeys(x))
head(mappedRkeys(x))

y=revmap(x)
summary(y)
direction(y)

length(y)
Llength(y)
Rlength(y)

head(keys(y))
head(Lkeys(y))
head(Rkeys(y))

count.mappedkeys(y)
count.mappedLkeys(y)
count.mappedRkeys(y)

head(mappedkeys(y))
head(mappedLkeys(y))
head(mappedRkeys(y))

z=subset(y, Lkeys=mappedLkeys(y), Rkeys=mappedRkeys(y))
length(z)
Llength(z)
Rlength(z)

