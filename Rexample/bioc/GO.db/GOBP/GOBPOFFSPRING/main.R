library(GO.db)

# Convert the object to a list
xx <- as.list(GOBPOFFSPRING)
# Remove GO IDs that do not have any ancestor
xx <- xx[!is.na(xx)]
if(length(xx) > 0) {
  # Get the ancestor GO IDs for the first two elents of xx
  goids <- xx[1:2]
}
goids
