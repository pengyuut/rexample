library(affy)

celpath=system.file("celfiles", package="affydata")
fns=list.celfiles(path=celpath, full.names=T)

abatch=ReadAffy(filenames=fns[1])
abatch
abatch=ReadAffy(filenames=fns[2])
abatch
abatch=ReadAffy(celfile.path=celpath)
abatch

