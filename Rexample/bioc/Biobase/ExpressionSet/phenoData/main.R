library('Biobase') 
nFeatures=10
nSamples=4
exprs=matrix(runif(nFeatures*nSamples), nrow=nFeatures, ncol=nSamples)

set.seed(0)

varlabels=c('gender', 'type')
pData=data.frame(
    sample(c('Female', 'Male'), nSamples, replace=T)
    , sample(c('Control', 'Case'), nSamples, replace=T)
    )
names(pData)=varlabels
row.names(pData)=LETTERS[1:nSamples]

metadata=data.frame(
    labelDescription=c('Patient gender', 'Case/control status')
    , row.names=names(pData)
    )

pData
metadata

phenoData=new('AnnotatedDataFrame', data=pData, varMetadata=metadata)

eset=new('ExpressionSet', exprs=exprs, phenoData=phenoData)
phenoData(eset)

