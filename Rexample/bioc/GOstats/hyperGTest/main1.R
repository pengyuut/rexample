suppressPackageStartupMessages(library(org.Mm.eg.db))
suppressPackageStartupMessages(library(GOstats))
universeGeneIds=sample(mappedkeys(org.Mm.egSYMBOL), 1000)
geneIds=sample(universeGeneIds, 100)

hgCutoff <- 0.001
params <- new("GOHyperGParams",
  geneIds=geneIds,
  universeGeneIds=universeGeneIds,
  #annotation="hgu95av2.db",
  ontology="BP",
  pvalueCutoff=hgCutoff,
  conditional=FALSE,
  testDirection="over")

hgOver=hyperGTest(params)


###################################################
### code chunk number 12: condHyperGeo
###################################################
paramsCond <- params
conditional(paramsCond) <- TRUE


###################################################
### code chunk number 13: standardHGTEST
###################################################
hgCondOver <- hyperGTest(paramsCond)

