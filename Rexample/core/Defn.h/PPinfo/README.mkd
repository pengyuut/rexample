~~~
typedef struct {
  PPkind kind;   /* deparse kind */
  PPprec precedence; /* operator precedence */
  unsigned int rightassoc;  /* right associative? */
} PPinfo;
~~~
