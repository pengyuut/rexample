~~~
/* The type definitions for the table of built-in functions. */
/* This table can be found in ../main/names.c */
typedef struct {
    char   *name;    /* print name */
    CCODE  cfun;     /* c-code address */
    int    code;     /* offset within c-code */
    int    eval;     /* evaluate args? */
    int    arity;    /* function arity */
    PPinfo gram;     /* pretty-print info */
} FUNTAB;
~~~

~~~
src/include/Defn.h
556:FUNTAB  R_FunTab[];     /* Built in functions */
~~~
