static void fmingr(int n, double *p, double *df, void *ex)
{
  SEXP s, x;
  int i;
  double val1, val2, eps, epsused, tmp;
  OptStruct OS = (OptStruct) ex;
  PROTECT_INDEX ipx;

  if (!isNull(OS->R_gcall)) { /* analytical derivatives */
    PROTECT(x = allocVector(REALSXP, n));
    if(!isNull(OS->names)) setAttrib(x, R_NamesSymbol, OS->names);
    for (i = 0; i < n; i++) {
      if (!R_FINITE(p[i]))
        error(_("non-finite value supplied by optim"));
      REAL(x)[i] = p[i] * (OS->parscale[i]);
    }
    SETCADR(OS->R_gcall, x);
    PROTECT_WITH_INDEX(s = eval(OS->R_gcall, OS->R_env), &ipx);
    REPROTECT(s = coerceVector(s, REALSXP), ipx);
    if(LENGTH(s) != n)
      error(_("gradient in optim evaluated to length %d not %d"),
          LENGTH(s), n);
    for (i = 0; i < n; i++)
      df[i] = REAL(s)[i] * (OS->parscale[i])/(OS->fnscale);
    UNPROTECT(2);
  } else { /* numerical derivatives */
    PROTECT(x = allocVector(REALSXP, n));
    setAttrib(x, R_NamesSymbol, OS->names);
    SET_NAMED(x, 2); // in case f tries to change it
    for (i = 0; i < n; i++) REAL(x)[i] = p[i] * (OS->parscale[i]);
    SETCADR(OS->R_fcall, x);
    if(OS->usebounds == 0) {
      for (i = 0; i < n; i++) {
        eps = OS->ndeps[i];
        REAL(x)[i] = (p[i] + eps) * (OS->parscale[i]);
        PROTECT_WITH_INDEX(s = eval(OS->R_fcall, OS->R_env), &ipx);
        REPROTECT(s = coerceVector(s, REALSXP), ipx);
        val1 = REAL(s)[0]/(OS->fnscale);
        REAL(x)[i] = (p[i] - eps) * (OS->parscale[i]);
        REPROTECT(s = eval(OS->R_fcall, OS->R_env), ipx);
        REPROTECT(s = coerceVector(s, REALSXP), ipx);
        val2 = REAL(s)[0]/(OS->fnscale);
        df[i] = (val1 - val2)/(2 * eps);
        if(!R_FINITE(df[i]))
          error(("non-finite finite-difference value [%d]"), i+1);
        REAL(x)[i] = p[i] * (OS->parscale[i]);
        UNPROTECT(1);
      }
    } else { /* usebounds */
      for (i = 0; i < n; i++) {
        epsused = eps = OS->ndeps[i];
        tmp = p[i] + eps;
        if (tmp > OS->upper[i]) {
          tmp = OS->upper[i];
          epsused = tmp - p[i];
        }
        REAL(x)[i] = tmp * (OS->parscale[i]);
        PROTECT_WITH_INDEX(s = eval(OS->R_fcall, OS->R_env), &ipx);
        REPROTECT(s = coerceVector(s, REALSXP), ipx);
        val1 = REAL(s)[0]/(OS->fnscale);
        tmp = p[i] - eps;
        if (tmp < OS->lower[i]) {
          tmp = OS->lower[i];
          eps = p[i] - tmp;
        }
        REAL(x)[i] = tmp * (OS->parscale[i]);
        REPROTECT(s = eval(OS->R_fcall, OS->R_env), ipx);
        REPROTECT(s = coerceVector(s, REALSXP), ipx);
        val2 = REAL(s)[0]/(OS->fnscale);
        df[i] = (val1 - val2)/(epsused + eps);
        if(!R_FINITE(df[i]))
          error(("non-finite finite-difference value [%d]"), i+1);
        REAL(x)[i] = p[i] * (OS->parscale[i]);
        UNPROTECT(1);
      }
    }
    UNPROTECT(1); /* x */
  }
}

