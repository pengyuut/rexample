/* par fn gr method options */
SEXP optim(SEXP call, SEXP op, SEXP args, SEXP rho)
{
  SEXP par, fn, gr, method, options, tmp, slower, supper;
  SEXP res, value, counts, conv;
  int i, npar=0, *mask, trace, maxit, fncount = 0, grcount = 0, nREPORT, tmax;
  int ifail = 0;
  double *dpar, *opar, val = 0.0, abstol, reltol, temp;
  const char *tn;
  OptStruct OS;
  PROTECT_INDEX par_index;

  args = CDR(args);
  OS = (OptStruct) R_alloc(1, sizeof(opt_struct));
  OS->usebounds = 0;
  OS->R_env = rho;
  par = CAR(args);
  OS->names = getAttrib(par, R_NamesSymbol);
  args = CDR(args); fn = CAR(args);
  if (!isFunction(fn)) error(_("'fn' is not a function"));
  args = CDR(args); gr = CAR(args);
  args = CDR(args); method = CAR(args);
  if (!isString(method)|| LENGTH(method) != 1)
    error(_("invalid '%s' argument"), "method");
  tn = CHAR(STRING_ELT(method, 0));
  args = CDR(args); options = CAR(args);
  PROTECT(OS->R_fcall = lang2(fn, R_NilValue));
  PROTECT_WITH_INDEX(par = coerceVector(par, REALSXP), &par_index);
  if (NAMED(par))
    REPROTECT(par = duplicate(par), par_index);
  npar = LENGTH(par);
  dpar = vect(npar);
  opar = vect(npar);
  trace = asInteger(getListElement(options, "trace"));
  OS->fnscale = asReal(getListElement(options, "fnscale"));
  tmp = getListElement(options, "parscale");
  if (LENGTH(tmp) != npar)
    error(_("'parscale' is of the wrong length"));
  PROTECT(tmp = coerceVector(tmp, REALSXP));
  OS->parscale = vect(npar);
  for (i = 0; i < npar; i++) OS->parscale[i] = REAL(tmp)[i];
  UNPROTECT(1);
  for (i = 0; i < npar; i++)
    dpar[i] = REAL(par)[i] / (OS->parscale[i]);
  PROTECT(res = allocVector(VECSXP, 5));
  SEXP names;
  PROTECT(names = allocVector(STRSXP, 5));
  SET_STRING_ELT(names, 0, mkChar("par"));
  SET_STRING_ELT(names, 1, mkChar("value"));
  SET_STRING_ELT(names, 2, mkChar("counts"));
  SET_STRING_ELT(names, 3, mkChar("convergence"));
  SET_STRING_ELT(names, 4, mkChar("message"));
  setAttrib(res, R_NamesSymbol, names);
  UNPROTECT(1);
  PROTECT(value = allocVector(REALSXP, 1));
  PROTECT(counts = allocVector(INTSXP, 2));
  SEXP countnames;
  PROTECT(countnames = allocVector(STRSXP, 2));
  SET_STRING_ELT(countnames, 0, mkChar("function"));
  SET_STRING_ELT(countnames, 1, mkChar("gradient"));
  setAttrib(counts, R_NamesSymbol, countnames);
  UNPROTECT(1);
  PROTECT(conv = allocVector(INTSXP, 1));
  abstol = asReal(getListElement(options, "abstol"));
  reltol = asReal(getListElement(options, "reltol"));
  maxit = asInteger(getListElement(options, "maxit"));
  if (maxit == NA_INTEGER) error(_("'maxit' is not an integer"));

  if (strcmp(tn, "Nelder-Mead") == 0) {
    double alpha, beta, gamm;

    alpha = asReal(getListElement(options, "alpha"));
    beta = asReal(getListElement(options, "beta"));
    gamm = asReal(getListElement(options, "gamma"));
    nmmin(npar, dpar, opar, &val, fminfn, &ifail, abstol, reltol,
        (void *)OS, alpha, beta, gamm, trace, &fncount, maxit);
    for (i = 0; i < npar; i++)
      REAL(par)[i] = opar[i] * (OS->parscale[i]);
    grcount = NA_INTEGER;

  }
  else if (strcmp(tn, "SANN") == 0) {
    tmax = asInteger(getListElement(options, "tmax"));
    temp = asReal(getListElement(options, "temp"));
    if (trace) trace = asInteger(getListElement(options, "REPORT"));
    if (tmax == NA_INTEGER) error(_("'tmax' is not an integer"));
    if (!isNull(gr)) {
      if (!isFunction(gr)) error(_("'gr' is not a function"));
      PROTECT(OS->R_gcall = lang2(gr, R_NilValue));
    } else {
      PROTECT(OS->R_gcall = R_NilValue); /* for balance */
    }
    samin (npar, dpar, &val, fminfn, maxit, tmax, temp, trace, (void *)OS);
    for (i = 0; i < npar; i++)
      REAL(par)[i] = dpar[i] * (OS->parscale[i]);
    fncount = npar > 0 ? maxit : 1;
    grcount = NA_INTEGER;
    UNPROTECT(1);  /* OS->R_gcall */

  } else if (strcmp(tn, "BFGS") == 0) {
    SEXP ndeps;

    nREPORT = asInteger(getListElement(options, "REPORT"));
    if (!isNull(gr)) {
      if (!isFunction(gr)) error(_("'gr' is not a function"));
      PROTECT(OS->R_gcall = lang2(gr, R_NilValue));
    } else {
      PROTECT(OS->R_gcall = R_NilValue); /* for balance */
      ndeps = getListElement(options, "ndeps");
      if (LENGTH(ndeps) != npar)
        error(_("'ndeps' is of the wrong length"));
      OS->ndeps = vect(npar);
      PROTECT(ndeps = coerceVector(ndeps, REALSXP));
      for (i = 0; i < npar; i++) OS->ndeps[i] = REAL(ndeps)[i];
      UNPROTECT(1);
    }
    mask = (int *) R_alloc(npar, sizeof(int));
    for (i = 0; i < npar; i++) mask[i] = 1;
    vmmin(npar, dpar, &val, fminfn, fmingr, maxit, trace, mask, abstol,
        reltol, nREPORT, (void *)OS, &fncount, &grcount, &ifail);
    for (i = 0; i < npar; i++)
      REAL(par)[i] = dpar[i] * (OS->parscale[i]);
    UNPROTECT(1); /* OS->R_gcall */
  } else if (strcmp(tn, "CG") == 0) {
    int type;
    SEXP ndeps;

    type = asInteger(getListElement(options, "type"));
    if (!isNull(gr)) {
      if (!isFunction(gr)) error(_("'gr' is not a function"));
      PROTECT(OS->R_gcall = lang2(gr, R_NilValue));
    } else {
      PROTECT(OS->R_gcall = R_NilValue); /* for balance */
      ndeps = getListElement(options, "ndeps");
      if (LENGTH(ndeps) != npar)
        error(_("'ndeps' is of the wrong length"));
      OS->ndeps = vect(npar);
      PROTECT(ndeps = coerceVector(ndeps, REALSXP));
      for (i = 0; i < npar; i++) OS->ndeps[i] = REAL(ndeps)[i];
      UNPROTECT(1);
    }
    cgmin(npar, dpar, opar, &val, fminfn, fmingr, &ifail, abstol,
        reltol, (void *)OS, type, trace, &fncount, &grcount, maxit);
    for (i = 0; i < npar; i++)
      REAL(par)[i] = opar[i] * (OS->parscale[i]);
    UNPROTECT(1); /* OS->R_gcall */

  } else if (strcmp(tn, "L-BFGS-B") == 0) {
    SEXP ndeps, smsg;
    double *lower = vect(npar), *upper = vect(npar);
    int lmm, *nbd = (int *) R_alloc(npar, sizeof(int));
    double factr, pgtol;
    char msg[60];

    nREPORT = asInteger(getListElement(options, "REPORT"));
    factr = asReal(getListElement(options, "factr"));
    pgtol = asReal(getListElement(options, "pgtol"));
    lmm = asInteger(getListElement(options, "lmm"));
    if (!isNull(gr)) {
      if (!isFunction(gr)) error(_("'gr' is not a function"));
      PROTECT(OS->R_gcall = lang2(gr, R_NilValue));
    } else {
      PROTECT(OS->R_gcall = R_NilValue); /* for balance */
      ndeps = getListElement(options, "ndeps");
      if (LENGTH(ndeps) != npar)
        error(_("'ndeps' is of the wrong length"));
      OS->ndeps = vect(npar);
      PROTECT(ndeps = coerceVector(ndeps, REALSXP));
      for (i = 0; i < npar; i++) OS->ndeps[i] = REAL(ndeps)[i];
      UNPROTECT(1);
    }
    args = CDR(args); slower = CAR(args); /* coerce in calling code */
    args = CDR(args); supper = CAR(args);
    for (i = 0; i < npar; i++) {
      lower[i] = REAL(slower)[i] / (OS->parscale[i]);
      upper[i] = REAL(supper)[i] / (OS->parscale[i]);
      if (!R_FINITE(lower[i])) {
        if (!R_FINITE(upper[i])) nbd[i] = 0; else nbd[i] = 3;
      } else {
        if (!R_FINITE(upper[i])) nbd[i] = 1; else nbd[i] = 2;
      }
    }
    OS->usebounds = 1;
    OS->lower = lower;
    OS->upper = upper;
    lbfgsb(npar, lmm, dpar, lower, upper, nbd, &val, fminfn, fmingr,
        &ifail, (void *)OS, factr, pgtol, &fncount, &grcount,
        maxit, msg, trace, nREPORT);
    for (i = 0; i < npar; i++)
      REAL(par)[i] = dpar[i] * (OS->parscale[i]);
    UNPROTECT(1); /* OS->R_gcall */
    PROTECT(smsg = mkString(msg));
    SET_VECTOR_ELT(res, 4, smsg);
    UNPROTECT(1);
  } else
    error(_("unknown 'method'"));

  if(!isNull(OS->names)) setAttrib(par, R_NamesSymbol, OS->names);
  REAL(value)[0] = val * (OS->fnscale);
  SET_VECTOR_ELT(res, 0, par); SET_VECTOR_ELT(res, 1, value);
  INTEGER(counts)[0] = fncount; INTEGER(counts)[1] = grcount;
  SET_VECTOR_ELT(res, 2, counts);
  INTEGER(conv)[0] = ifail;
  SET_VECTOR_ELT(res, 3, conv);
  UNPROTECT(6);
  return res;
}

