SEXP ndeps, smsg;
double *lower = vect(npar), *upper = vect(npar);
int lmm, *nbd = (int *) R_alloc(npar, sizeof(int));
double factr, pgtol;
char msg[60];

nREPORT = asInteger(getListElement(options, "REPORT"));
factr = asReal(getListElement(options, "factr"));
pgtol = asReal(getListElement(options, "pgtol"));
lmm = asInteger(getListElement(options, "lmm"));
if (!isNull(gr)) {
  if (!isFunction(gr)) error(_("'gr' is not a function"));
  PROTECT(OS->R_gcall = lang2(gr, R_NilValue));
} else {
  PROTECT(OS->R_gcall = R_NilValue); /* for balance */
  ndeps = getListElement(options, "ndeps");
  if (LENGTH(ndeps) != npar)
    error(_("'ndeps' is of the wrong length"));
  OS->ndeps = vect(npar);
  PROTECT(ndeps = coerceVector(ndeps, REALSXP));
  for (i = 0; i < npar; i++) OS->ndeps[i] = REAL(ndeps)[i];
  UNPROTECT(1);
}
args = CDR(args); slower = CAR(args); /* coerce in calling code */
args = CDR(args); supper = CAR(args);
for (i = 0; i < npar; i++) {
  lower[i] = REAL(slower)[i] / (OS->parscale[i]);
  upper[i] = REAL(supper)[i] / (OS->parscale[i]);
  if (!R_FINITE(lower[i])) {
    if (!R_FINITE(upper[i])) nbd[i] = 0; else nbd[i] = 3;
  } else {
    if (!R_FINITE(upper[i])) nbd[i] = 1; else nbd[i] = 2;
  }
}
OS->usebounds = 1;
OS->lower = lower;
OS->upper = upper;
lbfgsb(npar, lmm, dpar, lower, upper, nbd, &val, fminfn, fmingr,
    &ifail, (void *)OS, factr, pgtol, &fncount, &grcount,
    maxit, msg, trace, nREPORT);
for (i = 0; i < npar; i++)
REAL(par)[i] = dpar[i] * (OS->parscale[i]);
UNPROTECT(1); /* OS->R_gcall */
PROTECT(smsg = mkString(msg));
SET_VECTOR_ELT(res, 4, smsg);
UNPROTECT(1);
