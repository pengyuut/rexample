/* par fn gr options */
SEXP optimhess(SEXP call, SEXP op, SEXP args, SEXP rho)
{
  SEXP par, fn, gr, options, tmp, ndeps, ans;
  OptStruct OS;
  int npar, i , j;
  double *dpar, *df1, *df2, eps;

  args = CDR(args);
  OS = (OptStruct) R_alloc(1, sizeof(opt_struct));
  OS->usebounds = 0;
  OS->R_env = rho;
  par = CAR(args);
  npar = LENGTH(par);
  OS->names = getAttrib(par, R_NamesSymbol);
  args = CDR(args); fn = CAR(args);
  if (!isFunction(fn)) error(_("'fn' is not a function"));
  args = CDR(args); gr = CAR(args);
  args = CDR(args); options = CAR(args);
  OS->fnscale = asReal(getListElement(options, "fnscale"));
  tmp = getListElement(options, "parscale");
  if (LENGTH(tmp) != npar)
    error(_("'parscale' is of the wrong length"));
  PROTECT(tmp = coerceVector(tmp, REALSXP));
  OS->parscale = vect(npar);
  for (i = 0; i < npar; i++) OS->parscale[i] = REAL(tmp)[i];
  UNPROTECT(1);
  PROTECT(OS->R_fcall = lang2(fn, R_NilValue));
  PROTECT(par = coerceVector(par, REALSXP));
  if (!isNull(gr)) {
    if (!isFunction(gr)) error(_("'gr' is not a function"));
    PROTECT(OS->R_gcall = lang2(gr, R_NilValue));
  } else {
    PROTECT(OS->R_gcall = R_NilValue); /* for balance */
  }
  ndeps = getListElement(options, "ndeps");
  if (LENGTH(ndeps) != npar) error(_("'ndeps' is of the wrong length"));
  OS->ndeps = vect(npar);
  PROTECT(ndeps = coerceVector(ndeps, REALSXP));
  for (i = 0; i < npar; i++) OS->ndeps[i] = REAL(ndeps)[i];
  UNPROTECT(1);
  PROTECT(ans = allocMatrix(REALSXP, npar, npar));
  dpar = vect(npar);
  for (i = 0; i < npar; i++)
    dpar[i] = REAL(par)[i] / (OS->parscale[i]);
  df1 = vect(npar);
  df2 = vect(npar);
  for (i = 0; i < npar; i++) {
    eps = OS->ndeps[i]/(OS->parscale[i]);
    dpar[i] = dpar[i] + eps;
    fmingr(npar, dpar, df1, (void *)OS);
    dpar[i] = dpar[i] - 2 * eps;
    fmingr(npar, dpar, df2, (void *)OS);
    for (j = 0; j < npar; j++)
      REAL(ans)[i * npar + j] = (OS->fnscale) * (df1[j] - df2[j])/
        (2 * eps * (OS->parscale[i]) * (OS->parscale[j]));
    dpar[i] = dpar[i] + eps;
  }
  // now symmetrize
  for (i = 0; i < npar; i++) 
    for (j = 0; j < i; j++) {
      double tmp =
        0.5 * (REAL(ans)[i * npar + j] + REAL(ans)[j * npar + i]);
      REAL(ans)[i * npar + j] = REAL(ans)[j * npar + i] = tmp;
    }
  SEXP nm = getAttrib(par, R_NamesSymbol);
  if(!isNull(nm)) {
    SEXP dm;
    PROTECT(dm = allocVector(VECSXP, 2));
    SET_VECTOR_ELT(dm, 0, duplicate(nm));
    SET_VECTOR_ELT(dm, 1, duplicate(nm));
    setAttrib(ans, R_DimNamesSymbol, dm);
    UNPROTECT(1);
  }
  UNPROTECT(4);
  return ans;
}
