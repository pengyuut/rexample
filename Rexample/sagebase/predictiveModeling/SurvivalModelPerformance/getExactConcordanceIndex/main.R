suppressPackageStartupMessages(library(predictiveModeling))
#suppressPackageStartupMessages(library(survival))
trainPredictions=-c(1,2,3)
clinicalSurvData=clinicalSurvData=Surv(c(1,3,2), c(1, 1, 1))
trainPredictions
clinicalSurvData
trainPerformance=SurvivalModelPerformance$new(trainPredictions, clinicalSurvData)
print(trainPerformance$getExactConcordanceIndex())

