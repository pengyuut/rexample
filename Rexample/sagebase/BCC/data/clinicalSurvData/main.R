load(system('pathresolve.sh -d test_data clinicalSurvData.RData', intern=T))
str(clinicalSurvData)
library(ggplot2)
time=clinicalSurvData[,1]
status=factor(clinicalSurvData[,2])
levels(status)=c('alive', 'dead')
p=qplot(time, fill=status)
ggsave(p, file='main.png')

