library(abind)
# Five different ways of binding together two matrices
x=matrix(1:6,2,3)
x
y=x+100
y
z=abind(x,y,rev.along=1) # binds on last dimension
dim(z)
z
z=abind(x,y,rev.along=0) # binds on new dimension after last
dim(z)
z

