boot=function(
  data
  , statistic
  , R
  , ...
  ) {
  call=match.call()
  if (!exists(".Random.seed", envir = .GlobalEnv, inherits = FALSE)) runif(1)
  seed=get(".Random.seed", envir = .GlobalEnv, inherits = FALSE)
  n=NROW(data)
  if ((n == 0) || is.null(n))
    stop("no data in call to 'boot'")
  strata = rep(1, n)
  i=boot:::index.array(n, R, sim='ordinary', strata, m=0
    , L=NULL
    , weights=NULL
    )
  ns=tabulate(strata)[strata]
  t0=statistic(data, 1/ns, ...)

  f=freq.array(i)
  browser()
  f=f/ns
  fn=function(r) {
    statistic(data, f[r, ], ...)
  }

  RR=sum(R)
  res= lapply(seq_len(RR), fn)
  t.star=matrix(, RR, length(t0))
  for(r in seq_len(RR)) t.star[r, ]=res[[r]]

  boot:::boot.return(
    sim='ordinary', t0, t.star, strata=strata, R, data, statistic, stype='w', call, seed, L=NULL, m=0
    , pred.i=NULL
    , weights=1/tabulate(strata)[strata]
    , ran.gen = function(d, p) d
    , mle=NULL
    )
}

