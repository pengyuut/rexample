library(boot)
ratio=function(d, w) {
  sum(d$x * w)/sum(d$u * w)
}
boot(
  city
  , ratio
  , R = 999
  , stype = 'w'
  )

