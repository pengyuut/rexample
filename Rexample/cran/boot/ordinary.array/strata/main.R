n=10
R=20
strata=c(
  rep(1,n/2)
  , rep(2,n/2)
  )
set.seed(0)
boot:::ordinary.array(n=n, R=R, strata=strata)

