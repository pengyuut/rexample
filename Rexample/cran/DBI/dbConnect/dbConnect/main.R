library(RMySQL)
conn=dbConnect(MySQL(),user='genome',host='genome-mysql.cse.ucsc.edu',dbname='mm9')
dbDisconnect(conn)

library(RSQLite)
conn=dbConnect(SQLite(),dbname='main.db3')
dbDisconnect(conn)
