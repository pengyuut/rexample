## computing a simple ROC curve (x-axis: fpr, y-axis: tpr)
library(ROCR)
N0=1000
N1=1000
tmp=rbind(
    data.frame(prediction=rnorm(N0,mean=0), label=0)
    , data.frame(prediction=rnorm(N1,mean=1), label=1)
    )

pred=prediction(tmp$prediction, tmp$label)

perf=performance(pred,"tpr","fpr")
plot(perf)
## precision/recall curve (x-axis: recall, y-axis: precision)
perf1=performance(pred, "prec", "rec")
plot(perf1)
## sensitivity/specificity curve (x-axis: specificity,
## y-axis: sensitivity)
perf1=performance(pred, "sens", "spec")
plot(perf1)

#‘ppv’: Positive predictive value. P(Y = + | Yhat = +). Estimated as: TP/(TP+FP).
#‘npv’: Negative predictive value. P(Y = - | Yhat = -). Estimated as: TN/(TN+FN).

perf1=performance(pred, "ppv", "npv")
plot(perf1)

