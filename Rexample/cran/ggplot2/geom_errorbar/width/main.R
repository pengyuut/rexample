library(ggplot2)
x=1:6
tmp=data.frame(
  x=paste('x', x, sep='')
  , ymin=x-1/x
  , ymax=x+1/x
  , color=c(
    rep('color1',3)
    , rep('color2',3)
    )
  )
tmp$class <- row.names(tmp)
p=ggplot(tmp, aes(x=x)) + geom_errorbar(aes(ymin=ymin, ymax=ymax, width=.1, color=color))
ggsave(p, file=outfile, title='')

