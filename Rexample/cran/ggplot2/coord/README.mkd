~~~
coord <- function(..., subclass = c()) {
  structure(list(...), class = c(subclass, "coord"))
}
~~~
