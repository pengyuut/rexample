library(ggplot2)
x=1:6
tmp=data.frame(
  x=paste('x', x, sep='')
  , ymin=x-1/x
  , ymax=x+1/x
  )
tmp$class <- row.names(tmp)
p=ggplot(tmp, aes(x=x)) + geom_linerange(aes(ymin=ymin, ymax=ymax))
ggsave(p, file=outfile, title='')

