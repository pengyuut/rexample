library(ggplot2)
p=qplot(mpg, wt, data = mtcars, colour = factor(cyl)) scale_colour_manual(values = c("red","blue", "green"))
ggsave(p, file='main.png')
