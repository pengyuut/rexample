library(ggplot2)
dframe=data.frame(
    x=do.call(
        c
        , lapply(
            0:10
            , function(i) {rep(i*1000,10)}
            )
        )
    , y=do.call(
        c
        , lapply(
            0:10
            , function(i) {rnorm(10,i)}
            )
        )
    , z=as.factor(sample(1:3,11*10, replace=T))
    )
#dframe=rbind(
#    dframe
#    , data.frame(
#        x=rep(-1,10)
#        , y= rnorm(10,-1)
#        , z=sample(1:3,10, replace=T)
#        )
#    )
#p=qplot(x,y,data=dframe,geom='boxplot', group=round(x))+facet_grid(z~.)
p=qplot(x,y,data=dframe,geom='boxplot', colour=z, group=round(x))
ggsave(p, file='main.png')

