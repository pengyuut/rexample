#!/usr/bin/env bash

for n in 1000000;
do
  time Rscript main.R $n
done
