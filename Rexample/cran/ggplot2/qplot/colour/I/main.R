library(ggplot2)

png('main.png')
qplot(carat, price, data=diamonds, colour=I(alpha('black', 1/255)))
dev.off()

