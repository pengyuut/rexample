dframe=do.call(
  rbind
  , lapply(
    1:3
    , function(k) {
      data.frame(
        k=k
        , x=1:10
        , y=k*(1:10)
        )
    }
    )
  )
suppressPackageStartupMessages(library(ggplot2))
p=ggplot()+geom_line(data=dframe, aes(x=x, y=y, color=factor(k)))
ggsave(p, file='main.png')

