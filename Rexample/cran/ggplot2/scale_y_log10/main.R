library(ggplot2)
#options(digits=22)
options(digits=7)

tmp=data.frame(x=1:10)
tmp$y=10^(tmp$x^2)/100
p=qplot(x, y, data=tmp)+scale_y_log10()+xlab('xx')+ylab('yy')
p
ggsave(p, file='main.png')

