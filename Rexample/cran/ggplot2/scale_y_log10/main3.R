library(ggplot2)
tmp=rbind(
  data.frame(x='a', y=rnorm(10, mean=300000, sd=1000))
  , data.frame(x='b', y=rnorm(10, mean=300000, sd=1000))
  )
library(scales)
p=qplot(x, log10(y), data = tmp) + ylab(expression(log[10]~y))
ggsave(p, file='main.png')

