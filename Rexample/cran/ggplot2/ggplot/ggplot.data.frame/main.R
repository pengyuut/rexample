ggplot.data.frame <- function(data, mapping=aes(), ..., environment = globalenv()) {
  if (!missing(mapping) && !inherits(mapping, "uneval")) stop("Mapping should be created with aes or aes_string")

  p <- structure(list(
      data = data, 
      layers = list(),
      scales = Scales$new(),
      mapping = mapping,
      options = list(),
      coordinates = coord_cartesian(),
      facet = facet_null(),
      plot_env = environment
      ), class="ggplot")

  p$options$labels <- make_labels(mapping)

  set_last_plot(p)
  p
}


