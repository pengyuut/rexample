Because of the following code, only the source code in `src/`.

~~~
250:    instdir <- file.path(lib, pkg_name)
262:    owd <- setwd(instdir)
653:        owd <- setwd("src")
668:          srcs <- dir(pattern = "\\.([cfmM]|cc|cpp|f90|f95|mm)$",
669:            all.files = TRUE)
~~~

~~~
            setwd(owd)
~~~

