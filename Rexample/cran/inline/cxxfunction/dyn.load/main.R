dyn.load('test.so')
test=function(xx) {
  .Call('test', xx)
}

suppressPackageStartupMessages(library(inline))
test_inline=cxxfunction(
  signature(xx='numeric')
  , body='
  Rcpp::NumericMatrix x(xx);
  Rcpp::NumericMatrix result(x.nrow(), x.ncol()+1);

  for(unsigned i = 0; i < x.nrow(); ++ i) {
    double row_max=0;
    for(unsigned j = 0; j < x.ncol(); ++ j) {
      if(x(i, j) > row_max) row_max = x(i, j);
    }
    double row_exp_sum=0;
    for(unsigned j = 0; j < x.ncol(); ++ j) {
      double tmp = exp(x(i, j) - row_max);
      result(i,j) = tmp;
      row_exp_sum += tmp;
    }
    double tmp = exp(-row_max);
    result(i, x.ncol()) = tmp;
    row_exp_sum += tmp;

    for(unsigned j = 0; j < result.ncol(); ++ j) {
      result(i,j) /= row_exp_sum;
    }
  }
  return wrap(result);
  '
  , plugin='Rcpp'
  , verbose=T
  )


xx=matrix(1:6, nrow=2)

library(microbenchmark)
microbenchmark(test(xx), test_inline(xx), .Call('test', xx), times=1000)
library(rbenchmark)
benchmark(test(xx), test_inline(xx), .Call('test', xx), replications=1000)

