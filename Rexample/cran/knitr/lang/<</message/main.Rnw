% -*- mode: noweb; noweb-default-code-mode: R-mode; -*-
\documentclass{article}

\begin{document}
\begin{figure}
%<<cars-demo,dev='tikz',fig.width=4,fig.height=2.8,out.width='.45\\textwidth',cache=TRUE>>=
<<cars-demo,dev='tikz',message=FALSE>>=
library(ggplot2)
qplot(1:10, 1:10)
@
\caption{my caption}
\end{figure}

\end{document}
