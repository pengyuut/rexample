library(lattice)
g <- expand.grid(x = 1:10, y = 5:15, gr = 1:2)
g$z <- log((g$x^g$g + g$y^2) * g$gr)
png('main.png')
wireframe(z ~ x * y, data = g, groups = gr,
  scales = list(arrows = F),
  drape = T, colorkey = T,
  screen = list(z = 30, x = -60))
dev.off()
