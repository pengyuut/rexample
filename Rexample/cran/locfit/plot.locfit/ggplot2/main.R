suppressPackageStartupMessages(library(locfit))
tmp=do.call(
  rbind
  , lapply(
    seq(from=-1, to=1, length.out=100)*2*pi
    , function(x) {
      #browser()
      data.frame(
        x=x
        , y=rnorm(10, mean=sin(x))
        )
    }
    )
  )

fit=locfit(y~lp(x,nn=0.5), data=tmp)
library(ggplot2)
qplot(x, y, data=tmp)+geom_line(
    data=data.frame(
      x=unique(tmp$x)
      , y=predict(fit, unique(tmp$x))
      )
  , aes(x=x, y=y)
  , color='blue'
  )

qplot(unique(tmp$x), predict(fit, unique(tmp$x)))

#plot(fit, get.data=T)
plot(fit)

# fit with 50% nearest neighbor bandwidth.
fit <- locfit(NOx~lp(E,nn=0.5),data=ethanol)
plot(fit, get.data=T)
# bivariate fit.
fit <- locfit(NOx~lp(E,C,scale=T),data=ethanol)
plot(fit, get.data=T)

# density estimation
data(geyser)
fit <- locfit.raw(lp(geyser,nn=0.1,h=0.8))
plot(fit, get.data=T)

