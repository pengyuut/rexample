suppressPackageStartupMessages(library(locfit))
tmp=do.call(
  rbind
  , lapply(
    1:10
    , function(x) {
      data.frame(
        x=x
        , y=rnorm(10, mean=x)
        )
    }
    )
  )

fit=locfit(y~lp(x), data=tmp)
fit$mi

param=expand.grid(
  x=1:10
  , y=1:10
  )
tmp=do.call(
  rbind
  , lapply(
    seq_len(nrow(param))
    , function(i) {
      x=param[i, 'x']
      y=param[i, 'y']
      data.frame(
        x=x
        , y=y
        , z=rnorm(10, mean=x+y)
        )
    }
    )
  )

fit=locfit(z~lp(x, y), data=tmp)
fit$mi
