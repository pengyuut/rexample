library(locfit)
data(ethanol)
#fit=locfit(NOx~E, data=ethanol, alpha=0.5)
fit=locfit(ethanol$NOx~ethanol$E, alpha=0.5)
pdf(outfile)
plot(fit,get.data=T)
dev.off()
