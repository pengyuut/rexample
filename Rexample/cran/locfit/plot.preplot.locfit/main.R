suppressPackageStartupMessages(library(locfit))
tmp=do.call(
  rbind
  , lapply(
    1:10
    , function(i) {
      data.frame(
        x=i
        , y=rnorm(10, mean=i)
        )
    }
    )
  )
fit=locfit(y~x, data=tmp, alpha=0.5)
pred=locfit:::preplot.locfit(
  fit
  , newdata=seq(from=.6, to=1.2, length.out=10)
  , band='global'
  , tr=NULL
  , what='coef'
  , get.data=F
  , f3d=F # not available in R
  )
png('main.png')
locfit:::plot.preplot.locfit(pred)
dev.off()

