library(locfit)
x=1:100
y=1:100
fit=locfit(y~x, alpha=0.5)
predict(fit)
predict(fit, newdata=x)
fit=locfit(y~x+1, alpha=0.5)
predict(fit)
predict(fit, newdata=x)

x=sqrt(x)
fit=locfit(y~x*x, alpha=0.5)
predict(fit)
predict(fit, newdata=x)

fit=locfit(y~lp(x*x), alpha=0.5)
predict(fit)
predict(fit, newdata=x)
predict(fit, newdata=x*x)
plot(fit, newdata=x)

