% -*- mode: noweb; noweb-default-code-mode: R-mode; -*-
\documentclass[a4paper]{article}

\title{A Test File}
\author{Friedrich Leisch}


\usepackage{a4wide}

\begin{document}

\maketitle

A simple example: the integers from 1 to 10 are
~~~~~~{.R}
 [1]  1  2  3  4  5  6  7  8  9 10
~~~~~~~~~~~


We can also emulate a simple calculator:
~~~~~~{.R}
> 1 + 1
[1] 2
> 1 + pi
[1] 4.141593
> sin(pi/2)
[1] 1
~~~~~~~~~~~


Now we look at Gaussian data:

~~~~~~{.R}
 [1] -1.44567965  0.96327861  1.51590557 -0.07782579  1.08901871  0.07656381 -0.23831163  2.17937782  1.00972905 -0.73488404  1.06473107  0.81302180 -0.91360723  1.64018190
[15]  0.70680093 -0.73324004 -0.45896785  2.26043158  1.65516308  0.13213209
	One Sample t-test

data:  x 
t = 2.1928, df = 19, p-value = 0.04097
alternative hypothesis: true mean is not equal to 0 
95 percent confidence interval:
 0.02390721 1.02647477 
sample estimates:
mean of x 
 0.525191 
~~~~~~~~~~~

Note that we can easily integrate some numbers into standard text: The
third element of vector \texttt{x} is 1.51590557355858, the
$p$-value of the test is 0.040968. % $

Now we look at a summary of the famous \texttt{iris} data set, and we
want to see the commands in the code chunks:



~~~~~~{.R}
> data(iris)
> summary(iris)
  Sepal.Length    Sepal.Width     Petal.Length    Petal.Width          Species  
 Min.   :4.300   Min.   :2.000   Min.   :1.000   Min.   :0.100   setosa    :50  
 1st Qu.:5.100   1st Qu.:2.800   1st Qu.:1.600   1st Qu.:0.300   versicolor:50  
 Median :5.800   Median :3.000   Median :4.350   Median :1.300   virginica :50  
 Mean   :5.843   Mean   :3.057   Mean   :3.758   Mean   :1.199                  
 3rd Qu.:6.400   3rd Qu.:3.300   3rd Qu.:5.100   3rd Qu.:1.800                  
 Max.   :7.900   Max.   :4.400   Max.   :6.900   Max.   :2.500                  
~~~~~~~~~~~



\begin{figure}[htbp]
  \begin{center}
~~~~~~{.R}
> library(graphics)
> pairs(iris)
~~~~~~~~~~~

![](Sweave-test-1-006.jpg)

     \caption{Pairs plot of the iris data.}
  \end{center}
\end{figure}

\begin{figure}[htbp]
  \begin{center}
~~~~~~{.R}
> boxplot(Sepal.Length~Species, data=iris)
~~~~~~~~~~~

![](Sweave-test-1-007.jpg)

    \caption{Boxplot of sepal length grouped by species.}
  \end{center}
\end{figure}

\end{document}
