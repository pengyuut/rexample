library(multicore)
p <- fork()
if (inherits(p, "masterProcess")) {
  cat("I'm a child! ", Sys.getpid(), "\n")
  exit(,"I was a child")
}
cat("I'm the master\n")
unserialize(readChildren(1.5))

