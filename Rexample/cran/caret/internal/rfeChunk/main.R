library(caret)

n=1000
p=20
p0=3
set.seed(1)
x=matrix(runif(n * p), nrow = n)

colnames(x)=c(
    paste('bogus', seq_len(p-p0), sep = '')
    , paste('real', seq_len(p0), sep = '')
    )
y=rowSums(x[,grep('^real', colnames(x)),drop=F])+rnorm(n,sd=.1)

normalization=preProcess(x)
x_norm=as.data.frame(predict(normalization, x))

#plot(x[,20],y)
#plot(x[,19],y)

subsets=c(1:5, 10, 15, 20)

set.seed(10)
ctrl=rfeControl(functions = lmFuncs
    , method = 'cv'
    , verbose = F
    #, returnResamp = 'final'
    )

#source('~/test_rfe.R')

sizes = subsets

x=x_norm
y=y
rfeControl = ctrl

if(nrow(x) != length(y)) stop("there should be the same number of samples in x and y")
numFeat <- ncol(x)
classLevels <- levels(y)

if(is.null(rfeControl$index)) {
  rfeControl$index <- switch(
      tolower(rfeControl$method)
      , cv = createFolds(y, rfeControl$number, returnTrain = T)
      , repeatedcv = createMultiFolds(y, rfeControl$number, rfeControl$repeats)
      , loocv = createFolds(y, length(y), returnTrain = T)
      , boot =, boot632 = createResample(y, rfeControl$number)
      , test = createDataPartition(y, 1, rfeControl$p)
      , lgocv = createDataPartition(y, rfeControl$number, rfeControl$p)
      )
}

if(is.null(names(rfeControl$index))) names(rfeControl$index) <- prettySeq(rfeControl$index)

sizeValues <- sort(unique(sizes))
sizeValues <- sizeValues[sizeValues <= ncol(x)]


result=caret:::rfeChunk(
    inTrain=rfeControl$index[[1]]
    , x = x
    , y = y
    , cntl = rfeControl
    , sizes = sizeValues
    )


