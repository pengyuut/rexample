library(caret)
#library(Hmisc)
#library(randomForest)

n=1000
p=20
p0=3
set.seed(1)
x=matrix(runif(n * p), nrow = n)

colnames(x)=c(
    paste('bogus', seq_len(p-p0), sep = '')
    , paste('real', seq_len(p0), sep = '')
    )
y=rowSums(x[,grep('^real', colnames(x)),drop=F])+rnorm(n,sd=.1)

normalization=preProcess(x)
x_norm=as.data.frame(predict(normalization, x))

#plot(x[,20],y)
#plot(x[,19],y)

subsets=c(1:5, 10, 15, 20, 25)

set.seed(10)
ctrl=rfeControl(functions = lmFuncs
    , method = 'cv'
    , verbose = F
    , returnResamp = 'final'
    )

result=rfe(x_norm, y,
    sizes = subsets
    , rfeControl = ctrl
    )

assign(outvar_name, result)

save(list=outvar_name, file=outfile)

