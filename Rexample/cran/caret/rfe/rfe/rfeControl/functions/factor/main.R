library(caret)
#library(Hmisc)
#library(randomForest)
library(mlbench2)

n=1000
set.seed(1)
tmp=friedman_sphere(n, n_class=2)
 
y=tmp$y
normalization=preProcess(tmp$x)
x_norm=as.data.frame(predict(normalization, tmp$x))

#plot(x[,20],y)
#plot(x[,19],y)

subsets=c(1:15, 20, 25)

set.seed(10)
library(multicore)
ctrl=rfeControl(functions = rfFuncs
    , method = 'cv'
    , verbose = F
    , computeFunction=mclapply
    , workers=16
    #, returnResamp = 'final'
    )

runtime=system.time(
    result<-rfe(x_norm, y,
        sizes = subsets
        , rfeControl = ctrl
        , mtry=3
        )
    )
print(runtime)

#print(result)

assign(outvar_name, result)

save(list=outvar_name, file=outfile)

