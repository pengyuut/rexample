library(pROC)
x=rbind(
     data.frame(response=1, predictor=rnorm(100,1))
    ,data.frame(response=0, predictor=rnorm(100,0))
    )
auc(x$response, x$predictor)



## Syntax (response, predictor):
#auc(aSAH$outcome, aSAH$s100b)
#
## With a roc object:
#rocobj <- roc(aSAH$outcome, aSAH$s100b)
## Full AUC:
#auc(rocobj)
## Partial AUC:
#auc(rocobj, partial.auc=c(1, .8), partial.auc.focus="se", partial.auc.correct=TRUE)
#
## Alternatively, you can get the AUC directly from roc():
#roc(aSAH$outcome, aSAH$s100b)$auc
#roc(aSAH$outcome, aSAH$s100b,
#    partial.auc=c(1, .8), partial.auc.focus="se",
#    partial.auc.correct=TRUE)$auc
#
