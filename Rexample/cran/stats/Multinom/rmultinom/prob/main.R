library(stats)
size=12
prob=c(0.1,0.2,0.8,.3)
x=rmultinom(n=10000, size=size, prob=prob)
rowMeans(x)/size
prob/sum(prob)

