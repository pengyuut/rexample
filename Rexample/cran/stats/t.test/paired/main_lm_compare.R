set.seed(0)
tmp=do.call(
  rbind
  , lapply(
    seq_len(1000)
    , function(i) {
      x=rnorm(3)
      y=rnorm(3)

      tmp=data.frame(
        x1=c(rep('a', 3), rep('b', 3))
        , x2=rep(c('u', 'v', 'w'), 2)
        , y=c(x, y)
        , stringsAsFactors=T
        )

      data.frame(
        ttest_pval=t.test(x,y, paired=T)$p.value
        , anova_pval=anova(
          lm(y~x2, data=tmp)
          , lm(y~x1+x2-1, data=tmp)
          )[2, 'Pr(>F)']
        )
    }
    )
  )

library(ggplot2)
qplot(ttest_pval, anova_pval, data=tmp)

