#!/usr/bin/env bash

Rscript main.R
Rscript main_vector_matrix.R
Rscript main_matrix_vector.R
Rscript main_matrix_matrix.R
