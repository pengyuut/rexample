x=c(3, 7)
alpha=cbind(1:10,10:1)

source('ddirmultinom.R')
identical(
  sapply(
  1:10
  , function(i) {
    ddirmultinom(x=x, alpha=alpha[i,], log=T)
  }
  )
, ddirmultinom(x=x, alpha=alpha, log=T)
)

