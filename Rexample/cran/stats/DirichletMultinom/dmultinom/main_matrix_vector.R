x=cbind(1:10,10:1)
prob=c(3,7)

source('dmultinom.R')
identical(
  sapply(
    1:10
    , function(i) {
      dmultinom(x=x[i,], prob=prob, log=T)
    }
    )
  , dmultinom(x=x, prob=prob, log=T)
  )

