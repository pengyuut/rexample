n=10
size=(1:n)*10
tmp=data.frame(
  s=size
  , y=rbinom(n, size, .5)
  , stringsAsFactors=T
  )

with(tmp, y/s)

glmfit=glm(cbind(y, s-y)~1, family=binomial, data=tmp)
glmfit$y

