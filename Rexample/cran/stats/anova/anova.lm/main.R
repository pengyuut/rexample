set.seed(0)

a=3
b=4

AB_effect=data.frame(
  name=paste(
    unlist(
      do.call(
        rbind
        , rep(list(paste('A', letters[1:a],sep='')), b)
        )
      )
    , unlist(
      do.call(
        cbind
        , rep(list(paste('B', letters[1:b],sep='')), a)
        )
      )
    , sep=':'
    )
  , value=rnorm(a*b)
  , stringsAsFactors=F
  )

max_n=10
n=sample.int(max_n, a*b, replace=T)

AB=mapply(function(name, n){rep(name,n)}, AB_effect$name, n)

Y=AB_effect$value[match(unlist(AB), AB_effect$name)]

Y=Y+a*b*rnorm(length(Y))

sub_fr=as.data.frame(do.call(rbind, strsplit(unlist(AB), ':')))
rownames(sub_fr)=NULL
colnames(sub_fr)=c('A', 'B')

fr=data.frame(Y=Y,sub_fr)

alm1=lm(Y ~ A,fr)
alm2=lm(Y ~ A*B,fr)

sum(alm1$residuals^2)
sum(alm2$residuals^2)

anova(alm1)
anova(alm2)
seq_model=anova(alm1, alm2)
seq_model

SS=seq_model[1, 'RSS']-seq_model[2, 'RSS']
SS
(SS/seq_model[2, 'Df'])/(seq_model[2, 'RSS']/seq_model[2, 'Res.Df'])

