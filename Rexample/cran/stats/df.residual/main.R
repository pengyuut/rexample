n=10
size=(1:n)*100
y=rbinom(n, size, .5)
glmfit=glm(cbind(y, size)~1, family=binomial)

glmfit$df.residual
df.residual(glmfit)
#df.residual.default(glmfit)
