set.seed(0)

x=rbind(
  cbind(
    rnorm(10,0,.5)
    , rnorm(10,0,.5)
    , rnorm(10,0,.5)
    )
  , cbind(
    rnorm(15,5,.5)
    , rnorm(15,5,.5)
    , rnorm(15,5,.5)
    )
  )

ahclust=hclust(dist(x))
plot(ahclust)

cutree(ahclust,k=1:10)
cutree(ahclust,h=4)

