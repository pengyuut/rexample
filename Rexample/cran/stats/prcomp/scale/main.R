set.seed(0)
m=10
n=4
X=replicate(n,rnorm(m))

prcomp_result=prcomp(X,scale=T)
str(prcomp_result)

prcomp_result$center
apply(X,2,mean)

svd_result=svd(
    apply(
        apply(X,2,function(x){x-mean(x)}),
        2,
        function(x){x/sd(x)}
        )
    )
str(svd_result)

prcomp_result$sdev
svd_result$d/sqrt(m-1)

prcomp_result$rotation
svd_result$v
