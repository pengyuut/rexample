#require(graphics)
fn=function(v) {
  x <- v[1]
  y <- v[2]
  100 * (y - x^2)^2 + (1 - x)^2
}
optim(c(-1.2,1), fn)

fn=function(v) {
  x <- v[1]
  y <- v[2]
  - (100 * (y - x^2)^2 + (1 - x)^2)
}
optim(c(-1.2,1), fn, control=list(fnscale=-1))

