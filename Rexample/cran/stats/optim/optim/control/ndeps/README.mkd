https://stat.ethz.ch/pipermail/r-help/2011-May/279178.html

ndeps is useful when finite difference is used.

~~~
 30     for (i = 0; i < n; i++) REAL(x)[i] = p[i] * (OS->parscale[i]);


 34         eps = OS->ndeps[i];
 35         REAL(x)[i] = (p[i] + eps) * (OS->parscale[i]);
 36         PROTECT_WITH_INDEX(s = eval(OS->R_fcall, OS->R_env), &ipx);
 37         REPROTECT(s = coerceVector(s, REALSXP), ipx);
 38         val1 = REAL(s)[0]/(OS->fnscale);
 39         REAL(x)[i] = (p[i] - eps) * (OS->parscale[i]);
 40         REPROTECT(s = eval(OS->R_fcall, OS->R_env), ipx);
 41         REPROTECT(s = coerceVector(s, REALSXP), ipx);
 42         val2 = REAL(s)[0]/(OS->fnscale);
 43         df[i] = (val1 - val2)/(2 * eps);
~~~

