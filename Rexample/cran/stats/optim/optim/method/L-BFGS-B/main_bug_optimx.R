options(digits=22)

f0=-5.019474209739826164878
fmax=-5.018207190891093461005
xmax=0.0003737373737373737478916

f=function(x){
  (f0-fmax)*((x/xmax)-1)^2+fmax
}
g=function(x){
  2*(f0-fmax)*((x/xmax)-1)/xmax
}
f(0)
f(xmax)

array=seq(0, 2*xmax, length.out=1000)
plot(array, f(array))

suppressPackageStartupMessages(library(optimx))
optresult=optimx(0, f, lower=0, upper=Inf, method='L-BFGS-B', control=list(fnscale=-1))
optresult$par
optresult=optimx(xmax, f, lower=0, upper=Inf, method='L-BFGS-B', control=list(fnscale=-1))
optresult$par

optresult=optimx(0, f, g, lower=0, upper=Inf, method='L-BFGS-B', control=list(fnscale=-1))
optresult$par
optresult=optimx(xmax, f, g, lower=0, upper=Inf, method='L-BFGS-B', control=list(fnscale=-1))
optresult$par
