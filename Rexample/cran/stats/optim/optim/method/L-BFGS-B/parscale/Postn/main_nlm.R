#suppressPackageStartupMessages(library(pytools))

A=readRDS('Postn_hessian.rds')
par=readRDS('Postn_par.rds')
posdef=function (A) 
{
    A = (A + t(A))/2
    r = eigen(A)
    V = r$vectors
    lam = r$values
    V %*% diag(abs(lam)) %*% solve(V)
}

A=posdef(A)
fn=function(x) {
  drop((x - par) %*% A %*% (x - par))
}

result=nlm(
  fn
  , rep(0, length(par))
  , hessian=T
  #, control=list(
  #  ndeps=rep(1e-7, length(par))
  #  , parscale=c(rep(1,2), 1/100, rep(1, length(par)-3))
  #  , maxit=.Machine$integer.max
  #  #, factr=0
  #  )
  )
#result$counts
result

