set.seed(0)
m=10
n=2
x=data.frame(matrix(rnorm(m*n), nrow=m, ncol=n))
colnames(x)=c('v1', 'v2')
label=letters[1:m]

png('x.png')
plot(x)
text(x+c(.05,.05), label)
dev.off()

d=dist(x)
v=cmdscale(d)
png('d.png')
plot(v)
text(v+c(.05,.05), label)
dev.off()

