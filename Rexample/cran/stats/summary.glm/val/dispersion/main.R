n=10
size=(1:n)*100
tmp=data.frame(
  s=rep(size, 2)
  , y=c(
    rbinom(n, size, .3)
    , rbinom(n, size, .7)
    )
  , x=c(rep('a', n), rep('b', n))
  , stringsAsFactors=T
  )

glmfit1=glm(cbind(y, s-y)~1, family=binomial, data=tmp)
glmfit2=glm(cbind(y, s-y)~x, family=binomial, data=tmp)
summary(glmfit1)$dispersion
summary(glmfit2)$dispersion

