set.seed(0)

a=3
b=4

AB_effect=data.frame(
  name=paste(
    unlist(
      do.call(
        rbind
        , rep(list(paste('A', letters[1:a],sep='')), b)
        )
      )
    , unlist(
      do.call(
        cbind
        , rep(list(paste('B', letters[1:b],sep='')), a)
        )
      )
    , sep=':'
    )
  , value=rnorm(a*b)
  , stringsAsFactors=F
  )

max_n=10
n=sample.int(max_n, a*b, replace=T)

AB=mapply(function(name, n){rep(name,n)}, AB_effect$name, n)

Y=AB_effect$value[match(unlist(AB), AB_effect$name)]

Y=Y+a*b*rnorm(length(Y))

sub_fr=as.data.frame(do.call(rbind, strsplit(unlist(AB), ':')))
rownames(sub_fr)=NULL
colnames(sub_fr)=c('A', 'B')

fr=data.frame(Y=Y,sub_fr)

alm=lm(Y ~ 0,fr)
RSS0=sum(alm$residuals^2)
alm=lm(Y ~ A - 1,fr)
RSS_A=sum(alm$residuals^2)
alm=lm(Y ~ A+B - 1,fr)
RSS_AplusB=sum(alm$residuals^2)

RSS0-RSS_A
RSS_A-RSS_AplusB
RSS_AplusB

anova(alm)

