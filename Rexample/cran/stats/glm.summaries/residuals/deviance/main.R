n=10
size=(1:10)*n
y=rbinom(n, size, .5)
glmfit=glm(cbind(y, size)~1, family=binomial)

sum(residuals(glmfit, type='deviance')^2)
glmfit$deviance


