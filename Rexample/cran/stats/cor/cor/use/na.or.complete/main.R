set.seed(0)
n=4
x=rep(NA, 4)
y=rnorm(n)
cor(x,y, use='na.or.complete')

z=data.frame(rep(NA, 4)
    , rnorm(n)
    , rnorm(n)
    )
cor(z, use='na.or.complete')
