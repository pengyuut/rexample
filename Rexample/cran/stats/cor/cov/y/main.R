n=4
x=rnorm(n)
all.equal(drop(cov(as.matrix(x))), cov(x, x))
