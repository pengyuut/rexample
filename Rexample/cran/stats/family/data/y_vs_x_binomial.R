library(ggplot2)
eta=((-100):100)*.05
mu=sapply(
    eta
    , function(eta) {
      binomial()$linkinv(eta)
    }
    )

x=eta
y=sapply(
    mu
    , function(prob) {
      size=10
      rbinom(1, size, prob)/size
    }
    )


assign(outvar_name, data.frame(x=x, y=y))
save(list=outvar_name, file=outfile)
