library(ggplot2)
eta=((-100):100)*.05
mu=sapply(
    eta
    , function(eta) {
      binomial()$linkinv(eta)
    }
    )

tmp=data.frame(
    eta=eta
    , mu=mu
    )

p=qplot(eta, mu, data=tmp)
ggsave(p, file=outfile)

