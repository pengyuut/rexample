~~~ 
> fml=Gamma()
> str(fml)
List of 12
 $ family    : chr "Gamma"
 $ link      : chr "inverse"
 $ linkfun   :function (mu)  
 $ linkinv   :function (eta)  
 $ variance  :function (mu)  
 $ dev.resids:function (y, mu, wt)  
 $ aic       :function (y, n, mu, wt, dev)  
 $ mu.eta    :function (eta)  
 $ initialize:  expression({     if (any(y <= 0))          stop("non-positive values not allowed for the gamma family")     n <- rep.int(1, nobs)     mustart <- y })
 $ validmu   :function (mu)  
 $ valideta  :function (eta)  
 $ simulate  :function (object, nsim)  
 - attr(*, "class")= chr "family"
> 
~~~

Values that are different with different `link` parameters. $\mu$ is the mean and $\eta=\boldsymbol{X}\beta$.

- `linkfun`: $\eta(\mu)$

~~~
> Gamma(link='inverse')$linkfun
function (mu) 
1/mu
<bytecode: 0x102fa28b0>
<environment: 0x102fa30e8>
> Gamma(link='identity')$linkfun
function (mu) 
mu
<bytecode: 0x102fa1040>
<environment: 0x102fad8e8>
> Gamma(link='log')$linkfun
function (mu) 
log(mu)
<bytecode: 0x102fa1cd8>
<environment: 0x102fb6b18>
~~~

- `linkinv`: $\mu(\eta)$

~~~
> Gamma(link='inverse')$linkinv
function (eta) 
1/eta
<bytecode: 0x102fa3740>
<environment: 0x102fa30e8>
> Gamma(link='identity')$linkinv
function (eta) 
eta
<bytecode: 0x102fa1f78>
<environment: 0x102fad8e8>
> Gamma(link='log')$linkinv
function (eta) 
pmax(exp(eta), .Machine$double.eps)
<bytecode: 0x102fa1bf8>
<environment: 0x102fb6b18>
~~~

- `mu.eta`: $\frac{\mathrm{d} \mu}{\mathrm{d} \eta}$.

~~~
> Gamma(link='inverse')$mu.eta
function (eta) 
-1/(eta^2)
<bytecode: 0x102fa3628>
<environment: 0x102fa30e8>
> Gamma(link='identity')$mu.eta
function (eta) 
rep.int(1, length(eta))
<bytecode: 0x102fa1f08>
<environment: 0x102fad8e8>
> Gamma(link='log')$mu.eta
function (eta) 
pmax(exp(eta), .Machine$double.eps)
<bytecode: 0x102fa1958>
<environment: 0x102fb6b18>
~~~

- `valideta`

~~~
> Gamma(link='inverse')$valideta
function (eta) 
all(eta != 0)
<bytecode: 0x102fa3388>
<environment: 0x102fa30e8>
> Gamma(link='identity')$valideta
function (eta) 
TRUE
<bytecode: 0x102fa1d48>
<environment: 0x102fad8e8>
> Gamma(link='log')$valideta
function (eta) 
TRUE
<bytecode: 0x102fa2660>
<environment: 0x102fb6b18>
~~~


The same for all the `link`s:

- `variance`
- `dev.resids`
- `aic`
- `initialize`
- `validmu`

~~~
> Gamma(link='inverse')$validmu
function (mu) 
all(mu > 0)
<bytecode: 0x102f86c08>
<environment: 0x102f7b7f8>
~~~


