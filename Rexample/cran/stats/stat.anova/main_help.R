counts <- c(18,17,15,20,10,20,25,13,12)
outcome <- gl(3,1,9)
treatment <- gl(3,3)
glm.D93 <- glm(counts ~ outcome + treatment, family=poisson())
print(ag <- anova(glm.D93))
result=stat.anova(
  ag$table
  , test = "Cp"
  , scale = sum(resid(glm.D93, "pearson")^2)/4
  , df.scale = 4
  , n = 9
  )

