set.seed(0)

source('aov.R')

number_of_As=c(3,2)
A_factor=paste('A', letters[1:length(number_of_As)], sep='')
A_effect=data.frame(
  factor=A_factor
  , effect=rnorm(length(number_of_As))
  )

A_factors=rep(A_factor,number_of_As)
A_effects=A_effect[match(A_factors, A_effect$factor),'effect']+rnorm(sum(number_of_As))

aframe=data.frame(Y=A_effects, A=A_factors)
aaov=aov(Y~A-1, aframe) 
summary(aaov)

