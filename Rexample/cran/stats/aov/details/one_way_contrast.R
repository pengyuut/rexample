a = 3
n = 4
A = as.vector(sapply(1:a,function(x){rep(x,n)}))
Y = A + rnorm(a*n)

A = as.factor(A)
aframe = data.frame(Y=Y, A=A)
aaov = aov(Y ~ A - 1, aframe)
summary(aaov)
X = model.matrix(aaov)

mse=summary(aaov)[[1]]['Residuals', 'Mean Sq']
df=summary(aaov)[[1]]['Residuals', 'Df']

B = cbind(aaov$coefficients)

c1 = rep(0, length(aaov$coefficients))
c1[grep("A1", names(aaov$coefficients))]= 1
c1[grep("A2", names(aaov$coefficients))]=-1
c1

C = cbind(c1)
CtB = t(C) %*% B

XtXinv = solve(t(X) %*% X)
covCtB = t(C) %*% XtXinv %*% C * mse

t.statistic = CtB / sqrt(diag(covCtB))
p.value = 2*(1-pt(abs(t.statistic), df))

p.value
