a=3
b=4
c=5
d=6

A=1:a
B=1:b
C=1:c
D=1:d

n=4

X=matrix(nr=a*b*c*d*n,nc=n)
colnames(X)=LETTERS[1:n]

for(i_a in 1:a-1) {
  for(i_b in 1:b-1) {
    for(i_c in 1:c-1) {
      for(i_d in 1:d-1) {
        for(i_n in 1:n-1) {
          X[(((i_a * b + i_b) * c + i_c) * d + i_d) * n + i_n + 1, ] = c(i_a+1, i_b+1, i_c+1, i_d+1)
        }
      }
    }
  }
}

set.seed(0)
Y=matrix(nr=a*b*c*d*n,nc=1)
for(i in 1:(a*b*c*d)) {
  for(i_n in 1:n-1) {
    fa=X[i,'A']
    fb=X[i,'B']
    fc=X[i,'C']
    fd=X[i,'D']

    #Y[(i-1)*n+i_n,1]= fa +fb +fc +fa*fb +fa*fc +fb*fc +fa*fb*fc + rnorm(1)
    Y[(i-1)*n+i_n,1]= rnorm(1)
  }
}

aframe = data.frame(
  A=as.factor(X[,'A'])
  , B=as.factor(X[,'B'])
  , C=as.factor(X[,'C'])
  , D=as.factor(X[,'D'])
  , Y)

afit=aov(Y ~ A * B * C * D, aframe)

summary(afit)
#afit$coefficients

stopifnot(summary(afit)[[1]]['A ', 'Df']==a-1)
stopifnot(summary(afit)[[1]]['B ', 'Df']==b-1)
stopifnot(summary(afit)[[1]]['C ', 'Df']==c-1)
stopifnot(summary(afit)[[1]]['D ', 'Df']==d-1)

stopifnot(summary(afit)[[1]]['A:B ', 'Df']==(a-1)*(b-1))
stopifnot(summary(afit)[[1]]['A:C ', 'Df']==(a-1)*(c-1))
stopifnot(summary(afit)[[1]]['A:D ', 'Df']==(a-1)*(d-1))
stopifnot(summary(afit)[[1]]['B:C ', 'Df']==(b-1)*(c-1))
stopifnot(summary(afit)[[1]]['B:D ', 'Df']==(b-1)*(d-1))
stopifnot(summary(afit)[[1]]['C:D ', 'Df']==(c-1)*(d-1))

stopifnot(summary(afit)[[1]]['A:B:C ', 'Df']==(a-1)*(b-1)*(c-1))
stopifnot(summary(afit)[[1]]['A:B:D ', 'Df']==(a-1)*(b-1)*(d-1))
stopifnot(summary(afit)[[1]]['A:C:D ', 'Df']==(a-1)*(c-1)*(d-1))
stopifnot(summary(afit)[[1]]['B:C:D ', 'Df']==(b-1)*(c-1)*(d-1))

stopifnot(summary(afit)[[1]]['A:B:C:D ', 'Df']==(a-1)*(b-1)*(c-1)*(d-1))

