set.seed(0)
tmp=data.frame(
  Y=rnorm(5)
  , X=rep(letters[1:2],c(3,2))
  , stringsAsFactors=T
  )
model.matrix(~I(5), tmp)
model.matrix(~I(rep(3, 5))+X, tmp)
model.matrix(~I(rep(3, 5))+X-1, tmp)
model.matrix(~0+X+I(rep(3, 5)), tmp)

lm(Y~I(rep(3, 5))+X-1, tmp)
