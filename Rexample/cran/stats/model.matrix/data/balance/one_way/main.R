set.seed(0)
data=data.frame(Y=rnorm(5), X=rep(letters[1:2],c(3,2)))
A=model.matrix(Y~X, data)
A
t(A) %*% A

A=model.matrix(Y~X, data, contrasts=list(X='contr.sum'))
A
t(A) %*% A

data=data.frame(Y=rnorm(6), X=rep(letters[1:2],c(3,3)))
A=model.matrix(Y~X, data, contrasts=list(X='contr.sum'))
A
t(A) %*% A

A=model.matrix(Y~X, data)
A
t(A) %*% A

