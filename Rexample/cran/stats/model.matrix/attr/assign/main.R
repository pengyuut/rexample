set.seed(0)
tmp=data.frame(
  y=0
  , expand.grid(
    x1=letters[1:2]
    , x2=LETTERS[1:3]
    )
  , stringsAsFactors=T
  , check.names=F
  )

model.matrix(~1, tmp)
model.matrix(y~x1, tmp)
model.matrix(y~x1-1, tmp)
model.matrix(y~x1*x2, tmp)


