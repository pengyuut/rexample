`?MArrayLM-class`

- `coefficients`: 'matrix' containing fitted coefficients or contrasts
- `stdev.unscaled`: 'matrix' containing unscaled standard deviations of the coefficients or contrasts
- `sigma`: 'numeric' vector containing residual standard deviations for each gene
- `df.residual`: 'numeric' vector containing residual degrees of freedom for each gene

