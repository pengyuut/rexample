x=seq_len(10)
y=seq_len(7)

png(outfile)
image(list(x=x, y=y, z=outer(x, y, function(i, j) { 2*i+j })))
dev.off()
