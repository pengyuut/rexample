tmp=lapply(
  1:3
  , function(x) {
    rnorm(100)
  }
  )

X=data.frame(tmp)
colnames(X)=letters[1:3]

png('main.png')
pairs(X)
dev.off()

