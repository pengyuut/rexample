setClass(
  Class='A'
  , slots=signature(
    x='numeric'
    )
  )

#f=function(object, x, ...) { }

setGeneric(
  'f'
  , function(object, x, ...)
    standardGeneric('f')
  )

setMethod(
  f='f'
  , signature=c('A', 'character')
  , definition=function(object, x, ...) {
    print('x is character')
  }
  )

setMethod(
  f='f'
  , signature=c('A', 'numeric')
  , definition=function(object, x, ...) {
    print('x is numeric')
  }
  )

showMethods(f)
