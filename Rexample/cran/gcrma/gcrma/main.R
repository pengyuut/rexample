library(affy)
celpath=system.file("celfiles", package="affydata")
celfiles=list.celfiles(celpath, full.names=T, recursive=T)
dat=ReadAffy(filenames=celfiles)
library(gcrma)
eset=gcrma(dat)
str(exprs(eset))

