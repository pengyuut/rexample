suppressPackageStartupMessages(library(tweedie))
# Generate random numbers
set.seed(314)
y=rtweedie(500, p=1.5, mu=1, phi=1)
# With index p between 1 and 2, this produces continuous
# data with exact zeros
x=rnorm( length(y), 0, 1) # Unrelated predictor
# With exact zeros, index p must be between 1 and 2
# Fit the tweedie distribution; expect p about 1.5
out=tweedie.profile( y~1, p.vec=seq(1.1, 1.9, length=9), do.plot=TRUE)
out$p.max
# Plot this distribution
tweedie.plot( seq(0, max(y), length=1000), mu=mean(y),
p=out$p.max, phi=out$phi.max)
# Fit the glm
require(statmod) # Provides tweedie family functions
summary(glm( y ~ x, family=tweedie(var.power=out$p.max, link.power=0) ))
