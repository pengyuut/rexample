library(depmixS4)
mydata=data.frame(
  V1=c(
    sample(c(rep(1,100),rep(0,300)))
    , sample(c(rep(1,300),rep(0,100)))
    )
  , V2=c(
    sample(c(rep(1,100),rep(0,300)))
    , sample(c(rep(1,300),rep(0,100)))
    )
  )
mod <- depmix(
  list(V1~1, V2~1)
  , data=mydata
  , nstates=2
  , family=list(
    multinomial('identity')
    , multinomial('identity')
    )
  , instart=1:2
  , trstart=1:4
  , respstart=1:8
  )
mod
npar(mod)
freepars(mod)

