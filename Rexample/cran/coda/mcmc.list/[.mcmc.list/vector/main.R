library(coda)
x=read.openbugs(stem)
assign(outvar_name, x[,c('alpha', 'beta')])
save(list=outvar_name, file=outfile)

