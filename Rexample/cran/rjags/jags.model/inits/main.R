library(rjags)
line.data=list(
  x=c(1, 2, 3, 4, 5)
  , Y=c(1, 3, 3, 3, 5)
  , N=5
  )


jags_model=jags.model('line.bug'
  , data=line.data
  , inits=list(
    list(
      .RNG.name='base::Wichmann-Hill'
      , .RNG.seed=100
      )
    , list(
      .RNG.name='base::Marsaglia-Multicarry'
      , .RNG.seed=10
      )
    )
  , n.chains=2
  )

jags_model$state(internal=T)

jags_model=jags.model('line.bug'
  , data=line.data
  , inits=jags_model$state(internal=T)
  , n.chains=2
  )
jags_model$state(internal=T)

#jags.samples(jags_model, variable.names=c('alpha', 'beta', 'sigma'), n.iter=1000)
