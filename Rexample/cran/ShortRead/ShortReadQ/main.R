library(ShortRead)

seq=ShortReadQ(sread=DNAStringSet(c('ATGC', 'atgc')), quality=FastqQuality(c('IIII', 'IIII')))

writeFastq(seq, 'main.fastq')
