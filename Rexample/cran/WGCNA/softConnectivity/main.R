library(WGCNA)
nSamples=10
nGenes=40
datExpr=matrix(runif(nSamples*nGenes, min=-1), nrow=nSamples, ncol=nGenes)
result=softConnectivity(datExpr)
str(result)
identical(
  colSums(adjacency(datExpr))-1
  , result
  )

