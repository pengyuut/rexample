library(WGCNA)
set.seed(0)
m=10
n=4
datExpr=matrix(runif(m*n, min=-1), nrow=m, ncol=n)

f=function(power) {
  all.equal(TOMsimilarity(adjacency(datExpr, power=power)), TOMsimilarityFromExpr(datExpr, power=power))
}
invisible(
  lapply(
    1:5
    , f
    )
  )
