library(WGCNA)

set.seed(0)
adjMat=c(
  c(1,-1)
  , c(-1,1)
  )
dim(adjMat)=c(2,2)

myTOMsimilarity=function(adjMat) {
  f=function(i,j) {
    (
      (sum(
          (adjMat[i,] * adjMat[j,])[-c(i,j)]
          )
        + adjMat[i,j]
        )
      / (
        min(
          sum(adjMat[i,-i]), sum(adjMat[j,-j])
          )
        + 1 - adjMat[i,j]
        )
      )
  }
  stopifnot(identical(nrow(adjMat), ncol(adjMat)))

  result=matrix(nrow=nrow(adjMat), ncol=ncol(adjMat))
  apply(
    expand.grid(1:nrow(adjMat), 1:ncol(adjMat))
    , 1
    , function(x) {
      result[x[[1]], x[[2]]]<<-f(x[[1]], x[[2]])
    }
    )
  diag(result)=1
  result
}

TOMsimilarity(adjMat, TOMType='signed')
myTOMsimilarity(adjMat)

all.equal(TOMsimilarity(adjMat, TOMType='signed'), myTOMsimilarity(adjMat))

