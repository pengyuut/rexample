load('../../data_set/main/train.RData')
load('../../data_set/main/test.RData')
library(ada)
glog=ada(y~., data=train, test.x=test[,-1], test.y=test[,1], iter=50, loss="l", type="gentle")
glog

glog=addtest(glog, test[,-1], test[,1]) 
glog
save.RData('glog', path=outdir)

