library(rjags)
library(R2jags)

N=10
m=10
theta=.5
y=rbinom(N,m,theta)
alpha=2
beta=2

jags.data=list('y', 'N', 'm', 'alpha', 'beta')
jags.params=c('theta')
jags.inits=function(){
  list('theta'=runif(1))
}
jagsfit=jags(data=jags.data, inits=jags.inits, jags.params, n.iter=5000, model.file='model.bug')

# display the output
print(jagsfit)
plot(jagsfit)
# traceplot
traceplot(jagsfit)
# or to use some plots in coda
# use as.mcmc to convert rjags object into mcmc.list
jagsfit.mcmc=as.mcmc(jagsfit)
## now we can use the plotting methods from coda
xyplot(jagsfit.mcmc)
densityplot(jagsfit.mcmc)
# if the model does not converge, update it!
jagsfit.upd=update(jagsfit, n.iter=1000)
print(jagsfit.upd)
plot(jagsfit.upd)
# or auto update it until it converges! see ?autojags for details
jagsfit.upd=autojags(jagsfit)
# to get DIC or specify DIC=TRUE in jags() or do the following
dic.samples(jagsfit.upd$model, n.iter=1000, type='pD')
# attach jags object into search path see 'attach.bugs' for details
attach.jags(jagsfit.upd)
# this will show a 3-way array of the bugs.sim object, for example:
mu
# detach jags object into search path see 'attach.bugs' for details
detach.jags()
# to pick up the last save session
# for example, load('RWorkspace.Rdata')
recompile(jagsfit)
jagsfit.upd=update(jagsfit)

