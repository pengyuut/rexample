#include <Rcpp.h>

using namespace Rcpp;
RcppExport SEXP callFunction(SEXP xSEXP, SEXP fSEXP) {
    NumericVector x = Rcpp::as<NumericVector>(xSEXP);
    Function f = Rcpp::as<Function>(fSEXP);
    NumericVector __result = f(x);
    return Rcpp::wrap(__result);
}
