All the arguments must be specified.
~~~
~/dvcs_src/rexample/Rexample/cran/base/Recall$ Rscript main_default_arg.R 
> acc=function(n, inc=1) {
+   if(n==0) {
+     0
+   } else {
+     inc+Recall(n-1)
+   }
+ }
> acc(10, 2)
[1] 11
> 
> acc=function(n, inc=1) {
+   if(n==0) {
+     0
+   } else {
+     inc+Recall(n-1, inc)
+   }
+ }
> acc(10, 2)
[1] 20
~~~

