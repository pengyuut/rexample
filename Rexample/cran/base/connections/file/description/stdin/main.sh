#!/usr/bin/env bash

set -v
cat main.csv | Rscript main.R
cat main.csv | Rcmd.sh -e 'source("main.R")'

