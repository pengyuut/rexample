#!/usr/bin/env bash

set -v
R -q --slave --no-save --no-restore --no-environ <<EOF
Sys.getenv('R_LIBS_SITE')
EOF

export R_LIBS_SITE=.:..
R -q --slave --no-save --no-restore --no-environ <<EOF
.Library.site
EOF

