#!/usr/bin/env bash

export R_LIBS=R_LIBS
export R_LIBS_USER=R_LIBS_USER
export R_LIBS_SITE=R_LIBS_SITE

mkdir -p "$R_LIBS"
mkdir -p "$R_LIBS_USER"
mkdir -p "$R_LIBS_SITE"

cat main.R | R --vanilla --slave

