#include <Rcpp.h>

RcppExport SEXP test(SEXP bold_eta_theta) {
  using namespace Rcpp;
  NumericMatrix x(bold_eta_theta);
  NumericMatrix result(x.nrow(), x.ncol()+1);

  for(unsigned i = 0; i < x.nrow(); ++ i) {
    double row_max=0;
    for(unsigned j = 0; j < x.ncol(); ++ j) {
      if(x(i, j) > row_max) row_max = x(i, j);
    }
    double row_exp_sum=0;
    for(unsigned j = 0; j < x.ncol(); ++ j) {
      double tmp = exp(x(i, j) - row_max);
      result(i,j) = tmp;
      row_exp_sum += tmp;
    }
    double tmp = exp(-row_max);
    result(i, x.ncol()) = tmp;
    row_exp_sum += tmp;

    for(unsigned j = 0; j < result.ncol(); ++ j) {
      result(i,j) /= row_exp_sum;
    }
  }
  return wrap(result);
}

