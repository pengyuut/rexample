n=100
p=10

X=matrix(rnorm(n*p), nrow=n, ncol=p)

s=svd(X)
s$d

X_center=apply(X,2, function(x) {x-mean(x)})
s_center=svd(X_center)
plot(s$d, s_center$d)

