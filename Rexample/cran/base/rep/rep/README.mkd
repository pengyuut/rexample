~~~
> rep(1,10)
 [1] 1 1 1 1 1 1 1 1 1 1
~~~

~~~
> rep(c(1,2),3)
[1] 1 2 1 2 1 2
~~~

~~~
> rep(c(1,2),c(3,5))
[1] 1 1 1 2 2 2 2 2
~~~

~~~
> rep(c(1,2),c(2,3,5))
Error in rep(c(1, 2), c(2, 3, 5)) : invalid 'times' argument
Execution halted
~~~

