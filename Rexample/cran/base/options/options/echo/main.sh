#!/usr/bin/env bash

set -v
Rscript --no-init-file main.R

R -q --no-save --no-restore --no-init-file <<EOF
print(options('echo'))
EOF

R -q --no-save --no-restore --no-init-file --slave <<EOF
print(options('echo'))
EOF

