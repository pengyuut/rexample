m=10
n=6
k=3

m=300000
n=6
k=30000

set.seed(0)
x=replicate(n,rnorm(m))
f=sample(1:k, size=m, replace=T)

mysplit.data.frame<-function(x,f) {
  print('processing data.frame')
  v=lapply(
      1:dim(x)[[2]]
      , function(i) {
        split(x[,i],f)
      }
      )

  w=lapply(
      seq(along=v[[1]])
      , function(i) {
        result=do.call(
            cbind
            , lapply(v,
                function(vj) {
                  vj[[i]]
                }
                )
            )
        colnames(result)=colnames(x)
        return(result)
      }
      )
  names(w)=names(v[[1]])
  return(w)
}

#u=split(as.data.frame(x),f)
#v=split.data.frame(as.data.frame(x),f)
#
#u
#v

system.time(split(as.data.frame(x),f))
system.time(mysplit.data.frame(as.data.frame(x),f))

