warning("testit")
warnings()

f=function() {
  warning("testit")
  1
}

options(warn=0)
result=try(f())
print(result)

options(warn=1)
result=try(f())
print(result)

options(warn=2)
result=try(f())
print(result)

