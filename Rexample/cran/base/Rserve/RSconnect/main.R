library(Rserve)
c <- RSconnect()
data(stackloss)
RSassign(c, stackloss)
RSeval(c, quote(library(MASS)))
RSeval(c, quote(rlm(stack.loss ~ ., stackloss)$coeff))
RSeval(c, 'getwd()')
image <- RSeval(c, quote(try({
  attach(stackloss)
  library(Cairo)
  Cairo(file='plot.png')
  plot(Air.Flow,stack.loss,col=2,pch=19,cex=2)
  dev.off()
  readBin('plot.png', 'raw', 999999)})))
if (inherits(image, 'try-error'))
  stop(image)
