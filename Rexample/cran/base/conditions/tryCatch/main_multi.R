f=function(i) {
  if(i==1) {
    warning('one')
    warning('two')
  }
  42
}
library(multicore)
mclapply(
  1:2
  , function(i) {
    tryCatch(
      list(result=f(i))
      , error = function(e) e
      , warning = function(w) {
        list(
          result=f(i)
          , warning=w
          )
      }
      )
  }
  )
