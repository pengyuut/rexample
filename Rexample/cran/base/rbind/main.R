x=matrix(1:10,nr=2)
x
y=matrix(-1:-10,nr=2)
y
rbind(x,y)

x=data.frame(x=letters[1:10],y=letters[11:20], stringsAsFactors=F)
y=data.frame(x=letters[1:10],y=letters[11:20], stringsAsFactors=F)

rownames(x)=x$x
rownames(y)=y$x

z=rbind(x,y)
str(z)
head(z)

do.call(
  rbind
  , list(x=x, y=y)
  )

