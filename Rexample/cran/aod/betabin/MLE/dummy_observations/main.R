suppressPackageStartupMessages(library(HMP))
alpha1=5
alpha2=5
shape=c(alpha1, alpha2)

suppressPackageStartupMessages(library(aod))
set.seed(0)

Nrs=lapply(
  c(0, 5, 10, 25)
  , function(k) {
    c(rep(500,10), rep(0, k))
  }
  )

names(Nrs)=sapply(
  Nrs
  , function(x) {
    length(x)
  }
  )

tmp=lapply(
  Nrs
  , function(Nrs) {
    lapply(
      seq_len(1000)
      , function(i) {
        data.frame(Dirichlet.multinomial(Nrs, shape))
      }
      )
  }
  )

library(multicore)
i=1
options(warn=2)
tmpfit=mclapply(
  tmp
  , function(x) {
    do.call(
      rbind
      , lapply(x
        , function(x) {
          print(i)
          i<<-i+1
          result=try(betabin(cbind(X1, X2)~1, random=~1, data=x)@param)
          if(inherits(result, 'try-error')) {
            NULL
          } else {
            result
          }
        }
        )
      )
  }
  )

library(LaplacesDemon)
library(ggplot2)
library(reshape)
logitphi_melt=melt(
  lapply(
    tmpfit
    , function(x) {
      logit(x[,'phi.(Intercept)'])
    }
    )
  )

save(list='logitphi_melt', file='logitphi_melt.RData')

p=qplot(value, data=logitphi_melt)+facet_grid(L1~.)
ggsave(p, file='main1.png')

#qplot(sapply(tmpfit, function(x) { x@param['(Intercept)']}))


