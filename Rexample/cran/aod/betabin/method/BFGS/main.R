suppressPackageStartupMessages(library(HMP))
alpha1=1000
alpha2=1000
shape=c(alpha1, alpha2)
Nrs=(1:10)*10

suppressPackageStartupMessages(library(aod))
set.seed(0)

tmp=lapply(
  seq_len(1000)
  , function(i) {
    data.frame(Dirichlet.multinomial(Nrs, shape))
  }
  )

i=1
tmpfit=lapply(
  tmp
  , function(x) {
    print(i)
    i<<-i+1
    betabin(cbind(X1, X2)~1, random=~1, data=x, method='BFGS')
  }
  )

