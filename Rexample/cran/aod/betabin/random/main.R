suppressPackageStartupMessages(library(HMP))
Nrs=(1:10)*10
shape=list(
  c(10, 10)
  , c(100, 100)
  )
tmp=do.call(
  rbind
  , lapply(
    seq_along(shape)
    , function(i) {
      data.frame(x=letters[[i]], Dirichlet.multinomial(Nrs, shape[[i]]))
    }
    )
  )

as.factor.data.frame=function(aframe1) {
  ind <- sapply(aframe1, is.character)
  aframe1[ind] <- lapply(aframe1[ind], factor) 
  aframe1
}

tmp=as.factor.data.frame(tmp)
str(tmp)
head(tmp)

a_tmp=subset(tmp, x=='a')[,-1]
sum(a_tmp[,'X1'])/sum(a_tmp)

b_tmp=subset(tmp, x=='b')[,-1]
sum(b_tmp[,'X1'])/sum(b_tmp)

suppressPackageStartupMessages(library(aod))
bbfit=betabin(cbind(X1, X2)~x, random=~x, data=tmp, method='BFGS')

suppressPackageStartupMessages(library(LaplacesDemon))
bbfit@param

invlogit(bbfit@param['(Intercept)'])
invlogit(sum(bbfit@param[c('(Intercept)', 'xb')]))

#alpha1/(alpha1+alpha2)

bbfit@param[c('phi.xa', 'phi.xb')]
sapply(
  shape
  , function(shape) {
    1/(sum(shape)+1)
  }
  )

