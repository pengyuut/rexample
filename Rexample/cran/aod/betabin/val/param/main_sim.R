suppressPackageStartupMessages(library(HMP))
alpha1=1000
alpha2=1000
shape=c(alpha1, alpha2)
Nrs=(1:10)*10

suppressPackageStartupMessages(library(aod))
set.seed(0)

tmp=lapply(
  seq_len(1000)
  , function(i) {
    data.frame(Dirichlet.multinomial(Nrs, shape))
  }
  )

#library(multicore)
tmpfit=lapply(
  tmp
  , function(x) {
    betabin(cbind(X1, X2)~1, random=~1, data=x)
  }
  )

library(ggplot2)
qplot(sapply(tmpfit, function(x) { x@param['phi.(Intercept)']}))
phi=1/(alpha1+alpha2+1)
phi

library(LaplacesDemon)
qplot(sapply(tmpfit, function(x) { invlogit(x@param['(Intercept)'])}))
alpha1/(alpha1+alpha2)

