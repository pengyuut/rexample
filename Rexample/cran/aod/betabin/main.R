suppressPackageStartupMessages(library(HMP))
alpha1=10
alpha2=100
shape=c(alpha1, alpha2)
Nrs=(1:10)*10
tmp=data.frame(Dirichlet.multinomial(Nrs, shape))

suppressPackageStartupMessages(library(aod))
bbfit=betabin(cbind(X1, X2)~1, random=~1, data=tmp, method='BFGS')

suppressPackageStartupMessages(library(LaplacesDemon))
invlogit(bbfit@param['(Intercept)'])
alpha1/(alpha1+alpha2)

bbfit@param['phi.(Intercept)']
1/(alpha1+alpha2+1)

