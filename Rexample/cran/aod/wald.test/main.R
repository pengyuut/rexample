library(HMP)
alpha1=10
alpha2=10
shape=c(alpha1, alpha2)
Nrs=(1:10)*10
data=data.frame(Dirichlet.multinomial(Nrs, shape))

library(aod)
bbfit=betabin(cbind(X1, X2)~1, random=~1, data=data)
result=wald.test(b = coef(bbfit), Sigma = vcov(bbfit), Terms=1)
str(result)
result$result$chi2['P']

