library(HMP)
alpha1=10
alpha2=10
shape=c(alpha1, alpha2)
Nrs=(1:10)*10
data=data.frame(Dirichlet.multinomial(Nrs, shape))

library(aod)
bbfit=betabin(cbind(X1, X2)~1, random=~1, data=data)
summary(bbfit)

#library(LaplacesDemon)
#invlogit(bbfit@param['(Intercept)'])
#bbfit@param['phi.(Intercept)']
#1/(alpha1+alpha2+1)

