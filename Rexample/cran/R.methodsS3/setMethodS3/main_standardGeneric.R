library(R.oo)

setMethodS3('colnames', 'xxx', function(x, ...) {
    print('xxx colnames');
  })


x=42
class(x)='xxx'
colnames(x)

setMethodS3('otherfunction', 'xxx', function(x, ...) {
    print('xxx otherfunction');
  })


x=42
class(x)='xxx'
colnames(x)
otherfunction(x)

