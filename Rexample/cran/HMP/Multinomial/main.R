suppressPackageStartupMessages(library(HMP))
### Generate a random vector of number of reads per sample
q <- seq(5000, 20000, by=50)
Nrs <- sample(q, size=25)

mypi <- c(0.4, 0.3, 0.2, .05, 0.04, .01)

mult_data1 <- Multinomial(Nrs, mypi)
mult_data1

