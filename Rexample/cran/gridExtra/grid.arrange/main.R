library(gridExtra)
library(ggplot2)
x=1:10
y=1:10

data=lapply(
  (-10:10)/10
  , function(k) {
    data.frame(x=x,y=y*k)
  }
  )

p=lapply(
  data
  , function(d) {
    qplot(x,y, data=d)
  }
  )


png('main.png')
do.call(
  grid.arrange
  , p
  )
dev.off()

