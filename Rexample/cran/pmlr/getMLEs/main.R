library(pmlr)
mn_observation=function(p) {
  #x=as.vector(rmultinom(n=1, size = 30, prob=c(0.1,0.2,0.8)))
  x=as.vector(rmultinom(n=1, size = 10, prob=p))
  rep(seq_along(x), x)
}

p1=c(0.1, 0.2, 0.8)
p2=c(0.8, 0.1, 0.2)
p3=c(0.2, 0.8, 0.1)

set.seed(0)
data=rbind(
  data.frame(x='a', obs=mn_observation(p1))
  , data.frame(x='b', obs=mn_observation(p2))
  , data.frame(x='c', obs=mn_observation(p3))
  )
data$obs=factor(LETTERS[data$obs])

x=model.matrix(obs~x-1, data)
y=pmlr:::indicator.matrix(model.response(model.frame(obs~x-1, data)))
wt=rep(1, nrow(y))
pmlr:::getMLEs(x, y, wt)

