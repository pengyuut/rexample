~~~
> library(pmlr)
> mn_observation=function(p) {
+   #x=as.vector(rmultinom(n=1, size = 30, prob=c(0.1,0.2,0.8)))
+   x=as.vector(rmultinom(n=1, size = 30, prob=p))
+   rep(seq_along(x), x)
+ }
> 
> p1=c(0.1,0.2,0.8)
> p2=c(0.8,0.1,0.2)
> 
> set.seed(0)
> data=rbind(
+   data.frame(x='a', obs=mn_observation(p1))
+   , data.frame(x='b', obs=mn_observation(p2))
+   )
> data$obs=factor(LETTERS[data$obs])
> 
> fit=pmlr(obs~x-1, data = data)
> summary(fit)
pmlr(formula = obs ~ x - 1, data = data)

Likelihood confidence intervals and p-values

, , B

         coef  se(coef) lower(coef) upper(coef)        OR  lower(OR) upper(OR) Chi-Square   Pr > ChiSq
xa -0.2006707 0.6407474   -1.514135   1.0591071 0.8181818 0.21999839 2.8837950  0.1001673 7.516289e-01
xb -1.9042375 0.5640969   -3.222585  -0.9063004 0.1489362 0.03985192 0.4040161 16.6031176 4.607522e-05

, , C

        coef  se(coef) lower(coef) upper(coef)        OR  lower(OR)  upper(OR) Chi-Square   Pr > ChiSq
xa  1.363305 0.4816768   0.4959587    2.407507 3.9090909 1.64207167 11.1062333   10.13326 0.0014561746
xb -1.652923 0.5142244  -2.8040339   -0.737353 0.1914894 0.06056526  0.4783785   14.12851 0.0001707367
~~~

