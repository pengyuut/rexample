~~~
> library(pracma)
> rosenbrock
function (x) 
{
    n <- length(x)
    x1 <- x[2:n]
    x2 <- x[1:(n - 1)]
    sum(100 * (x1 - x2^2)^2 + (1 - x2)^2)
}
<environment: namespace:pracma>
~~~

