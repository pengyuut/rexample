#install.packages('car', repos = 'http://R-Forge.R-project.org')
#install.packages('gridExtra', repos = 'http://R-Forge.R-project.org')
install.packages('graph', repos = 'http://R-Forge.R-project.org')

install.packages('mypkg', dependencies=T)

install.packages('ggplot2', dep=T, lib='/pearson/data/pengy/utility/R')
