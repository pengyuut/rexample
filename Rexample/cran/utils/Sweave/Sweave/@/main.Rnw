% -*- mode: noweb; noweb-default-code-mode: R-mode; -*-
\documentclass{article}

\begin{document}
<<>>=
print(1:20)

@ blah blah
blah blah

Hello World!
<<>>=
print(1:20)
@

<<>>=
1:20
<<>>=
letters[1:3]
@

\end{document}
