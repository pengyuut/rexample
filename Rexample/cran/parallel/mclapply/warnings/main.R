library(multicore)
options('warn')
f=function(i) {
  if(i %% 2) {
    warning(sprintf('%d', i))
  }
  i
}
unlist(mclapply(1:30, f))
warnings()
unlist(lapply(1:30, f))
warnings()
