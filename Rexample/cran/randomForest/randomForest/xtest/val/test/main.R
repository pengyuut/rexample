suppressPackageStartupMessages(library(randomForest))
x=matrix(runif(500), 100)
y=gl(2, 50)
xtest=matrix(runif(5e2), 100)
ytest=gl(2, 50)
rfresult=randomForest(x, y
  , xtest=xtest
  , ytest=ytest
  )

names(rfresult$test)
lapply(rfresult$test, head)
str(rfresult$test$predicted)
str(rfresult$test$err.rate)
str(rfresult$test$confusion)
str(rfresult$test$votes)
str(rfresult$test$proximity)

