library(gbm)
load('data1.RData')

gbm1=gbm(
    formula(data1)
    , data=data1                   # dataset
    # poisson, coxph, and quantile available
    , n.trees=3000                # number of trees
    , interaction.depth=3         # 1: additive model, 2: two-way interactions, etc.
    , n.minobsinnode = 10         # minimum total weight needed in each node
    , train.fraction = 0.5        # fraction of data for training,
    # first train.fraction*N used for training
    , shrinkage=0.005             # shrinkage or learning rate,
    # 0.001 to 0.1 usually work
    , bag.fraction = 0.5          # subsampling fraction, 0.5 is probably best
    , cv.folds = 5                # do 5-fold cross-validation
    , keep.data=T              # keep a copy of the dataset with the object
    , verbose=T)                # print out progress

assign(outvar_name, gbm1)
save(list=outvar_name, file=outfile)

