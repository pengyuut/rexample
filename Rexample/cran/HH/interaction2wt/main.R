## Artificial data with all interactions significant.

## The interaction2wt plot shows all main effects and all pairwise
## interactions.  We see in the "Y ~ A|B" panel (or in the
## interaction.plot) that Y goes uphill for levels 1 and 2 of B and
## goes down and then up for level 3 of B.  This is the two-way A:B
## interaction.  At each level of B, the main effect of A differs.

## From the xyplot, we see that at level 1 of C, Y goes down for level
## 3 of B.  At level 2 of C, Y goes down a lot and then up for level 3
## of B.  This is the three-way A:B:C interaction.  At each level of C
## the two-way interaction of A and B differs.


set.seed(1)

require(HH)  ## needed for interaction2wt()

threeway <-data.frame(
  matrix(
    c(
      1, 1, 1, 1,
      2, 2, 1, 1,
      3, 3, 1, 1,
      1, 1, 2, 1,
      2, 2, 2, 1,
      3, 3, 2, 1,
      3, 1, 3, 1,
      2, 2, 3, 1,
      1, 3, 3, 1,
      1, 1, 1, 2,
      2, 2, 1, 2,
      3, 3, 1, 2,
      1, 1, 2, 2,
      2, 2, 2, 2,
      3, 3, 2, 2,
      3, 1, 3, 2,
      -4, 2, 3, 2,
      1, 3, 3, 2
      )
    , byrow=TRUE, 18, 4
    , dimnames=list(1:18,c("Y","A","B","C"))
    )
  )

for (i in 2:4) {
  threeway[[i]] <- factor(threeway[[i]])
}

threeway <- rbind(threeway, threeway)

threeway$Y <- threeway$Y + rnorm(36, s=.5)


anova(aov(Y ~ A * B * C, data=threeway))

with(threeway, interaction.plot(A, B, Y))  ## shows just the "Y ~ A|B" panel

## all two-way interactions and main effects
interaction2wt(Y ~ A + B + C, data=threeway)  ## library(HH) required

xyplot(x ~ A | C, groups=B,
  data=aggregate(threeway[,1], threeway[,-1], mean), type="l",
  auto.key=list(title="B", space="left",
    border=TRUE, lines=TRUE, points=FALSE),
  strip=strip.custom(strip.names=c(TRUE,TRUE)))

