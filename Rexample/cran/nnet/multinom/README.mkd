[Multinomial logit](http://en.wikipedia.org/wiki/Multinomial_logit)

[Multinomial Response Models](http://data.princeton.edu/wws509/notes/c6.html)

[ordered logit model (a.k.a ordered logistic regression or proportional odds model)](http://en.wikipedia.org/wiki/Ordered_logit) for ordered data.

[main3.R]():
The residual deviance from a multinomial model is numerically equal (up to round-off error) to that you would get had you fitted the model as a surrogate Poisson generalized linear model.
[multinom() residual deviance](http://r.789695.n4.nabble.com/multinom-residual-deviance-td3436654.html)


[main_anova.R]()

