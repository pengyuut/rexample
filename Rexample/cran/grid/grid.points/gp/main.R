library(grid)
pdf('main.pdf', title='')
grid.points(
  x=unit(c(.5,.7),'npc')
  , y=unit(c(.5,.7),'npc')
  , pch=4
  , gp=gpar(col=c('blue', 'red'))
  )
dev.off()
