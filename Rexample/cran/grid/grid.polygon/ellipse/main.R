library(grid)
pdf('main.pdf', title='')
r=1
pushViewport(
  viewport(
    width = unit(.8, 'npc')
    , height = unit(.8, 'npc')
    , xscale=c(-r,r)
    , yscale=c(-r,r)
    )
  )
#grid.rect()
#grid.points(x=0,y=0)

Sigma=rbind(
  c(1,.5)
  , c(.5,1)
  )

Gamma=chol(Sigma)

n=36
theta=2*pi*seq_len(n)/n
Y=rbind(
  r*cos(theta)
  , r*sin(theta)
  )
X=t(Gamma)%*%Y

grid.polygon(
  x=X[1,]
  , y=X[2,]
  , default.units='native'
  )

dev.off()

