pdf(outfile, title='', width=10, height=10)
f=function(n) {
  pie(rep(1, n), col=topo.colors(n))
}
par(mfrow=c(2,3))
lapply(2^(1:6), f) 
dev.off()
